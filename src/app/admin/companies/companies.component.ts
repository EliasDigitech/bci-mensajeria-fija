import { Component, Renderer2, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ListCompany } from '../models';
import { CompaniesService } from './services/companies.services';
import { ActivatedRoute, Router } from "@angular/router";
import { DownloadService } from '../../services/download.service';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.scss']
})


export class CompaniesComponent implements OnInit {
  @ViewChild('companiesTable') companiesTable : ElementRef;

   listCompany : ListCompany [] = new  Array <ListCompany> ();

  constructor(
    private companiesService: CompaniesService,
    private router : Router,
    private route: ActivatedRoute,
    private renderer: Renderer2,
    private downloadService: DownloadService,
  ) { }

  dtOptions: any = {
    dom: 'Bfrtip',
    serverSide: true,
    processing: true,
    language: {
      url: '../../../assets/languages/Spanish.json'
    },
    buttons: {
      buttons:[
        // {
        // text: 'Crear',
        // key: '1',
        // className: 'btn btn-primary btn-sm',
        // action: (e, dt, node, config) => {
        //   this.router.navigate(['./new'],{relativeTo : this.route});
        //   }
        // },
        {
           text: 'Excel',
           key: '2',
           className: 'btn btn-success btn-sm text-white',
           action: (e, dt, node, config) => {
             let dtparams = dt.ajax.params()
              let params = { 
                report_type: 'excel',
                search: dtparams.search.value
              }
             this.downloadService.downloadReport('companies/reports/', params)
                .subscribe(
                  res => {
                    this.downloadFile('xlsx', res)
                  }
                )
           }
         },{
          text: 'PDF',
          key: '2',
          className: 'btn btn-danger btn-sm text-white',
          action: (e, dt, node, config) => {
            let dtparams = dt.ajax.params()
            let params = {
              report_type: 'pdf',
              search: dtparams.search.value
            }
            this.downloadService.downloadReport('companies/reports/', params)
              .subscribe(
                res => {
                  this.downloadFile('pdf', res)
                }
              )
          }
        },
       ]
    },
    columns: [
      { 
        title: 'Rut',     
        data: 'rut',
        render: function (data: any, type: any, full: any) {
          if(data){
          let dataLength = data.length
            return data.substr(0,dataLength-1)+'-'+data.substr(dataLength-1,1)
          }else{
            return data
          }
        }
    },
      { title: 'Razón social',
        data: 'name', 
        render: function (data: any, type: any, full: any) {
          if(data){
            return data[0].toUpperCase() + data.slice(1);
          }else{
            return data;
          }
        }
      },
      { title: 'Nombre fantasía',
        data: 'commercial_business',
        render: function (data: any, type: any, full: any) {
          if(data){
            return data[0].toUpperCase() + data.slice(1);
          }else{
            return data;
          }
        }
      }
      ,{
        title: 'Tipo empresa',
        data: 'company_type',
        render: function (data: any, type: any, full:any){
          if(data=='client'){
            return "Cliente"
          } else if (data=='distibutor') {
            return "Distribuidor"
          } else if (data == 'owner') {
            return "Principal"
          }else{
            return "No definido"
          }
        }
      },{
        title: '',
        data: 'id',
        orderable: false,
        className: 'text-center',
        render: function (data: any, type: any, full: any) {
          return `<button type="button" class="btn btn-info btn-sm" view-person-id="${data}">
                    <i class="fas fa-eye" view-person-id="${data}"></i>
                  </button>`;
        }
      },
    ],
    ajax : (data : any, callback ) => {
      let order = data.order[0];
      let consulta:any = {
        search: data.search.value,
        offset: data.start,
        limit : data.length,
        ordering: (order.dir == 'desc'?'-':'') + data.columns[order.column].data
      };
      
      this.companiesService.listcompanyParams(consulta)
      .subscribe(
        (response: any) => {
          // console.log(response);
          callback({
            recordsTotal : response.count,
            recordsFiltered : response.count,
            data : response.results
          });
        },
          error => {
            console.log(error);
        }
      )
    },
  };

  downloadFile(extension:string, res:Blob): void {
    const now = new Date();
    let anchor = document.createElement('a');
    anchor.download = "Empresas " + now.toLocaleString('es-CL') + "." + extension;
    anchor.href = window.URL.createObjectURL(res);
    anchor.dataset.downloadurl = ['text/plain', anchor.download, anchor.href].join(':');
    anchor.click();
  }

  ngAfterViewInit(): void {
    this.renderer.listen(this.companiesTable.nativeElement, 'click', (event) => {
      // console.log(event);
      // event.stopPropagation();
      if (event.target.hasAttribute("view-person-id")) {
        // alert(event.target.getAttribute("view-person-id"));
        // this.router.navigate(["companies" + event.target.getAttribute("view-person-id")]);
        this.router.navigate([event.target.getAttribute("view-person-id")],{ relativeTo : this.route });
      }
    });
  }

  editLis(id: number){
    this.router.navigate([id],{ relativeTo : this.route });
  }

  ngOnInit() {
   }


}
