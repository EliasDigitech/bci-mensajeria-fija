import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment'
import { CreateCompany, ListUsers, CreateBranch } from "./../../models";
import { XhrErrorHandlerService } from '../../../services/xhr-error-handler.service';
import { catchError } from 'rxjs/operators';

@Injectable()
export class CompaniesService {
    constructor(
        private http: HttpClient,
        private xhrErrorHandlerService: XhrErrorHandlerService
    ) { }

    // companies
    public listcompanyParams(consulta ?: any){
        return this.http.get(environment.server_ip + 'companies/', { params: consulta })
            .pipe(
                catchError(this.xhrErrorHandlerService.handleError)
            );
    }

    public viewCompany(id: number) {
        return this.http.get(environment.server_ip + 'companies/' + id + '/')
            .pipe(
                catchError(this.xhrErrorHandlerService.handleError)
            );
    }

    public editCompany(id: number, data: any) {
        return this.http.put(
            environment.server_ip + 'companies/' + id + '/', data
        )
        .pipe(
            catchError(this.xhrErrorHandlerService.handleError)
        );
    }

    public createCompany(paramCreateCompany: CreateCompany) {
        return this.http.post(
            environment.server_ip + 'companies/', paramCreateCompany
        )
        .pipe(
            catchError(this.xhrErrorHandlerService.handleError)
        );
    }


    // branches
    public listBranches(id: number, consulta?: any) {
        return this.http.get(
            environment.server_ip + 'companies/' + id + '/branches/', { params: consulta }
        ).pipe(
            catchError(this.xhrErrorHandlerService.handleError)
        );
    }

    public getBranch(idBranch:number, idCompany: number) {
        return this.http.get(
            environment.server_ip + 'companies/' + idCompany + '/branches/' + idBranch + '/'
        ).pipe(
            catchError(this.xhrErrorHandlerService.handleError)
        );
    }

    public createBranch(idCompany:number, createBranch: CreateBranch, ) {
        return this.http.post(
            environment.server_ip + 'companies/' + idCompany + '/branches/', createBranch
        ).pipe(
            catchError(this.xhrErrorHandlerService.handleError)
        );
    }

    public updateBranch(idCompany: number, idBranch: number, data: any) {
        return this.http.put(
            environment.server_ip + 'companies/' + idCompany + '/branches/' + idBranch + '/', data
        ).pipe(
            catchError(this.xhrErrorHandlerService.handleError)
        );
    }


    // users
    public listUsers(idCompany: number, consulta?: any) {
        return this.http.get(
            environment.server_ip + 'companies/' + idCompany + '/users/',
            { params: consulta }
        ).pipe(
            catchError(this.xhrErrorHandlerService.handleError)
        );
    }

    public getUser(companyId: Number, userId: Number) {
        return this.http.get(
            environment.server_ip + 'companies/' + companyId + '/users/' + userId + '/')
            .pipe(
                catchError(this.xhrErrorHandlerService.handleError)
            )
    }

    public retrieveUserTypes() {
        return this.http.get(environment.server_ip + 'render/groups/')
            .pipe(
                catchError(this.xhrErrorHandlerService.handleError)
            )
    }

    public createUser(company_id: number, data: any) {
        return this.http.post(
            environment.server_ip + 'companies/' + company_id + '/users/', data
        )
            .pipe(
                catchError(this.xhrErrorHandlerService.handleError)
            )
    }

    public updateUser(company_id: number, user_id: number, data: any) {
        return this.http.patch(
            environment.server_ip + 'companies/' + company_id + '/users/' + user_id + '/', data
        )
            .pipe(
                catchError(this.xhrErrorHandlerService.handleError)
            )
    }

}