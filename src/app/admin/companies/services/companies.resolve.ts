import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable, EMPTY } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CompaniesService } from './companies.services';

@Injectable()
export class CompanyResolver implements Resolve<any> {
    constructor(
        private companiesService: CompaniesService,
        private router: Router,
        ) { }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any> | Promise<any> | any {
        return this.companiesService.viewCompany(route.params['id'])
        .pipe(
           catchError( err => {
               this.router.navigate(['404']);
               return EMPTY;
           })
        )
    }
}

@Injectable()
export class BranchResolver implements Resolve<any> {
    constructor(
        private companiesService: CompaniesService,
        private router: Router,
    ) { }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any> | Promise<any> | any {
        if (route.params['branch_id'] === 'new') {
            return true;
        }
        return this.companiesService.getBranch(route.params['branch_id'], route.params['id'])
            .pipe(
                catchError(err => {
                    this.router.navigate(['404']);
                    return EMPTY;
                })
            )
    }
}

@Injectable()
export class UserResolver implements Resolve<any> {
    constructor(
        private companiesService: CompaniesService,
        private router: Router,
    ) { }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any> | Promise<any> | any {
        if (route.params['user_id'] === 'new') {
            return true;
        }
        return this.companiesService.getUser(route.params['id'], route.params['user_id'])
            .pipe(
                catchError(err => {
                    this.router.navigate(['404']);
                    return EMPTY;
                })
            )
    }
}
