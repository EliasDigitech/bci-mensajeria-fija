import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditLiscompanyComponent } from './edit-liscompany.component';

describe('EditLiscompanyComponent', () => {
  let component: EditLiscompanyComponent;
  let fixture: ComponentFixture<EditLiscompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditLiscompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditLiscompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
