import { Component, OnInit, Renderer2, ElementRef, ViewChild } from '@angular/core';
import { CompaniesService } from '../services/companies.services';
import { ListBranches, EditListCompany } from '../../models';
import { ActivatedRoute, Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { DownloadService } from '../../../services/download.service';
import { MetaTitleService } from '../../../services/meta-title.service';

const FIELDS = {
  branch: "Sucursal",
  email: "E-mail",
  first_name: "Nombres",
  last_name: "Apellidos",
  phone: "Teléfono",
  rut: "RUT",
  status: "Estado",
  username: "Nombre de Usuario",
}

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.scss']
})
export class ListUserComponent implements OnInit {

  @ViewChild('userTable') userTable : ElementRef;
  @ViewChild(DataTableDirective) dtElement : DataTableDirective;

  branchDetail: ListBranches = new ListBranches();
  companyDetail: EditListCompany = new EditListCompany();

  constructor(
    private companiesService: CompaniesService,
    private router: Router,
    private route: ActivatedRoute,
    private renderer: Renderer2,
    private downloadService: DownloadService,
    private metaTitleService: MetaTitleService,
  ) { 
    this.metaTitleService.updateTitle();
    this.route.data.subscribe(
      (data: any) => {
        this.branchDetail = data.branch;
        this.companyDetail = data.company;
      }
    )
  }

  ngOnInit() {
  }

  dtOptions: any = {
    dom: 'Bfrtip',
    serverSide: true,
    language: {
      url: '../../../assets/languages/Spanish.json'
    },
   processing: true,
     buttons: {
      buttons:[
        {
          text: 'Crear',
          key: '1',
          className: 'btn btn-primary btn-sm',
          action: (e, dt, node, config) => {
          this.router.navigate(['./new'],{relativeTo : this.route});
          }
        },{
           text: 'Excel',
           key: '2',
           className: 'btn btn-success btn-sm text-white',
           action: (e, dt, node, config) => {
             let dtparams = dt.ajax.params()
             let params = {
               report_type: 'excel',
               search: dtparams.search.value
             }
             const company = this.route.snapshot.params["id"]
             this.downloadService.downloadReport('companies/' + company + '/users/reports/', params)
               .subscribe(
                 res => {
                   this.downloadFile('xlsx', res)
                 }
               )
           }
         },{
          text: 'PDF',
          key: '2',
          className: 'btn btn-danger btn-sm text-white',
          action: (e, dt, node, config) => {
            let dtparams = dt.ajax.params()
            let params = {
              report_type: 'pdf',
              search: dtparams.search.value
            }
            const company = this.route.snapshot.params["id"]
            this.downloadService.downloadReport('companies/' + company + '/users/reports/', params)
              .subscribe(
                res => {
                  this.downloadFile('pdf', res)
                }
              )
          }
        },
       ]
    },
    columns: [
      { 
        title: 'Usuario',
        data: 'username'
      },{ 
        title: 'Nombre',
        data: 'first_name',
        render: function (data: any, type: any, full: any) {
          if(data){
            return data[0].toUpperCase() + data.slice(1);
          }else{
            return data;
          }
        }
      },{ 
        title: 'Apellido',
        data: 'last_name',
        render: function (data: any, type: any, full: any) {
          if(data){
            return data[0].toUpperCase() + data.slice(1);
          }else{
            return data;
          }
        }
      },{ 
        title: 'Rut',
        data: 'rut',
        render: function (data: any, type: any, full: any) {
          if(data){
          let dataLength = data.length;
            return data.substr(0,dataLength-1)+'-'+data.substr(dataLength-1,1)
          }else{
            return data;
          }
        }
      },{ 
        title: 'Teléfono',
        data: 'phone',
        render: function (data: any, type: any, full: any) {
          if(data){
            return  data.substr(0,2)+' '+data.substr(2,2) +' '+data.substr(4,3) +' '+data.substr(7,4) 
          }else{
            return data
          }
        }
      },{ 
        title: 'Email',
        data: 'email'
      },{ 
        title: 'Editar',
        data: 'id',
        orderable: false,
        className: 'text-center',
        render: function (data: any, type: any, full: any) {
          return `<button type="button" class="btn btn-sm btn-info" idEditUser="${data}"><i class="fas fa-edit" idEditUser="${data}"></i>
                </button>`
        }
      }
    ],
    ajax : (data : any, callback ) => {
      let order = data.order[0];
      let consulta: any = {
        search: data.search.value,
        offset: data.start,
        limit: data.length,
        ordering: (order.dir == 'desc' ? '-' : '') + data.columns[order.column].data
      };
      this.companiesService.listUsers(this.route.snapshot.params['id'], consulta)
      .subscribe(
        (response: any) => {
          callback({
            recordsTotal: response.count,
            recordsFiltered: response.count,
            data: response.results
          });
        },
        error => {
          console.log(error);
        }
      )
    },  
};

ngAfterViewInit(): void {
  this.renderer.listen(this.userTable.nativeElement, 'click', (event) => {
    // console.log(event);
    if (event.target.hasAttribute("idEditUser")) {
      this.router.navigate(
        [event.target.getAttribute("idEditUser")],
        { relativeTo: this.route }
      );
    }
  });
}

downloadFile(extension: string, res: Blob): void {
    const now = new Date();
    let anchor = document.createElement('a');
    anchor.download = "Usuarios " + now.toLocaleString('es-CL') + "." + extension;
    anchor.href = window.URL.createObjectURL(res);
    anchor.dataset.downloadurl = ['text/plain', anchor.download, anchor.href].join(':');
    anchor.click();
    document.removeChild(anchor);
  }
}
