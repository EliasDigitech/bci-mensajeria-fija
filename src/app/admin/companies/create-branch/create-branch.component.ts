import { Component, OnInit, ViewChild, ElementRef, NgZone, OnDestroy } from '@angular/core';
import { CreateBranch } from '../../models';
import { CompaniesService } from '../services/companies.services'
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, Validators } from '@angular/forms';
import { MapsAPILoader } from '@agm/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MetaTitleService } from '../../../services/meta-title.service';
const FIELDS = {
  name: "Nombre",
  address: "Dirección",
  phone: "Teléfono",
  contact_name: "Nombre contacto",
  comune: "Comuna",
  region: "Región",
  max_users: "Usuarios máximos",
}

@Component({
  selector: 'app-create-branch',
  templateUrl: './create-branch.component.html',
  styleUrls: ['./create-branch.component.scss']
})
export class CreateBranchComponent implements OnInit, OnDestroy {
  @ViewChild("addressSearch")
  public searchElementRef: ElementRef;
  public isNew: boolean;
  createBranchData: CreateBranch = new CreateBranch();
  destroy$: Subject<boolean> = new Subject<boolean>();
  isLoading: boolean = false;
  branchForm = this.fb.group({
    name: ['', Validators.required],
    address: ['', Validators.required],
    address_number: [''],
    phone: ['', Validators.required],
    contact_name: ['', Validators.required],
    province: ['', Validators.required],
    comune: ['', Validators.required],
    region: ['', Validators.required],
    max_users: [0],
    lat: [''],
    lng: [''],
  })

  constructor(
    private companiesService: CompaniesService,
    private route: ActivatedRoute,
    private router: Router,
    private toastrService: ToastrService,
    private fb: FormBuilder,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private metaTitleService: MetaTitleService,
  ) {
    this.metaTitleService.updateTitle();
    this.route.data
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(
        data => {
          if (data.branch === true) {
            this.isNew = true;
          } else {
            this.isNew = false;
            this.branchForm.patchValue(data.branch);
          }
        }
      )
  }

  ngOnInit() {
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(
        this.searchElementRef.nativeElement,
        {
          types: ["address"],
          componentRestrictions: { country: "cl" }
        }
      );
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          let components = place.address_components;
          let addressValues = {
            address: place.name,
            comune: components.find(el => el.types.includes("administrative_area_level_3")).long_name,
            province: components.find(el => el.types.includes("administrative_area_level_2")).long_name,
            region: components.find(el => el.types.includes("administrative_area_level_1")).long_name,
            lat: Math.round(place.geometry.location.lat() * 10000000) / 10000000,
            lng: Math.round(place.geometry.location.lng() * 10000000) / 10000000,
          }
          this.branchForm.patchValue(addressValues);
        })
      });
    })
    this.branchForm.controls['address'].valueChanges
      .pipe(
        takeUntil(
          this.destroy$
        )
      )
      .subscribe(
        () => {
          this.branchForm.controls['comune'].reset()
          this.branchForm.controls['province'].reset()
          this.branchForm.controls['region'].reset()
        }
      )
  }

  createBranchSave() {
    this.isLoading = true;
    let idCompany = this.route.snapshot.params['id']
    this.companiesService.createBranch(idCompany, this.branchForm.value)
      .pipe(
        takeUntil(this.destroy$)
      ).subscribe(
        (response: any) => {
          // console.log(response.results);
          this.isLoading = false;
          this.toastrService.success('Nueva sucursal creada', 'Datos ingresados correctamente', { timeOut: 6000 });
          this.router.navigate(['../'], { relativeTo: this.route });
        },
        error => {
          this.isLoading = false;
          if (error.errMsg && error.status != 500) {
            for (let errHead in error.errMsg) {
              this.toastrService.error(error.errMsg[errHead], FIELDS[errHead]);
            }
          } else {
            this.toastrService.error("Ha ocurrido un problema");
          }
        }

      )
  }

  updateBranch() {
    this.isLoading = true;
    let idCompany = this.route.snapshot.params['id']
    let idBranch = this.route.snapshot.params['branch_id']
    this.companiesService.updateBranch(idCompany, idBranch, this.branchForm.value)
      .pipe(
        takeUntil(this.destroy$)
      ).subscribe((response: any) => {
        this.isLoading = false;
        this.toastrService.success('', 'Cambios realizados correctamente');
        this.router.navigate(['../'], { relativeTo: this.route });
      },
        error => {
          this.isLoading = false;
          if (error.errMsg && error.status != 500) {
            for (let errHead in error.errMsg) {
              this.toastrService.error(error.errMsg[errHead], FIELDS[errHead]);
            }
          } else {
            this.toastrService.error("Ha ocurrido un problema");
          }
        }
      )
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}
