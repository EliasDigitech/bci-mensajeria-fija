import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, Validators, FormArray, FormControl} from '@angular/forms';
import { CreateCompany, ListUsers } from './../../models';
import { CompaniesService } from  '../services/companies.services';
import { ToastrService } from "ngx-toastr";
import { rutValidator } from '../../../extra/validators';
import { LayoutService } from '../../../layouts/services/layout.service';
import { switchMap } from 'rxjs/operators';
import { forkJoin } from 'rxjs';
import { Pricings} from '../../../admin/models';
import { MetaTitleService } from '../../../services/meta-title.service';

const FIELDS =  {
  "name": "Razón social",
  "address": "Dirección",
  "commercial_business": "Nombre Fantasía",
  "phone": "Teléfono",
  "contact_name": "Contacto",
  "comune": "Comuna",
  "province": "Provincia",
  "region": "Región",
  "max_users": "Usuarios",
  "company": "Empresa"
}

@Component({
  selector: 'app-create-company',
  templateUrl: './create-company.component.html',
  styleUrls: ['./create-company.component.scss']
})
export class CreateCompanyComponent implements OnInit {

  paramCreateCompany : CreateCompany = new CreateCompany(); 
  isEditing: boolean = false;
  inputDisable: boolean = false;
  executivesList: Array<any>;
  loading: boolean = false;
  canEditTrader: boolean = false;
  canEditExtras: boolean = true;
  segments; 
  services;
  pricings : Pricings [] = new Array <Pricings> ();
  

  companyForm = this.fb.group({
    name: ['', Validators.required],
    commercial_business: ['', Validators.required],
    rut: ['', [Validators.required, rutValidator(), Validators.minLength(7)]],
    company_type: ['client', Validators.required],
    max_users: ['', Validators.required],
    max_branches: ['', Validators.required],
    phone: ['', Validators.required],
    email: ['', Validators.required],
    contact_name: ['', Validators.required],
    status: ['active', Validators.required],
    trader : this.fb.group({ 
      first_name: [''],
      email: [''],
      phone: [''],
      trader: [''],
      company: [''],
    }),
    billing_contacts : this.fb.array([]),
    company_price : this.fb.array([]),
  })

  constructor(
    private companiesService: CompaniesService, 
    private toastrService : ToastrService, 
    private Router : Router,
    private fb: FormBuilder,
    private actr: ActivatedRoute,
    private layoutService: LayoutService,
    private metaTitleService: MetaTitleService,
  ) {
    this.metaTitleService.updateTitle();
    this.actr.parent.data.subscribe(
      (data) => {
        if(data.group === "dHJhZGVycw=="){
          this.companyForm.controls["company_type"].disable();
        }
        if(data.group === "dXNlcnM="){
          this.canEditExtras = false;
        }
        if (data.group === 'YWRtaW5z'){
          this.canEditTrader = true;
          this.listExecute();
        }else{
          this.trader.get('trader').disable()
        }
      }
    )
    this.actr.data.subscribe(
      res => {
        if (actr.snapshot.params.id){
          let company: CreateCompany =  res.company;
          for(let bill of company.billing_contacts){
            this.addBillingInfo();
          }
          if (!company.trader){
            company.trader = new ListUsers();
          }
          this.companyForm.patchValue(company);
          console.log(this.companyForm.value)
          this.inputDisable=true
          this.isEditing = true;
        }
      }
    )
  }

  ngOnInit() {

    this.ViewPricings();
    forkJoin(
      this.layoutService.segments({ company_default: this.actr.snapshot.params.id }),
      this.layoutService.services({ company_default: this.actr.snapshot.params.id })
    )
    .subscribe(
      (responses: any) => {
        this.segments = responses[0].results;
        this.services = responses[1].results;
        for(let segment of this.segments){
          for(let service of this.services){
            this.addFilePricingInfo(segment.id, service.id);
          }
        }
        this.mostrarPricing();
      }
    )

  }
  
  getCurrentIndex(y:number, x:number){
    return (y + x) + ((this.services.length - 1)*y);
  }

  get company_price(){
    return this.companyForm.get('company_price') as FormArray
  } 

  addFilePricingInfo(segment:number, service:number){
    this.company_price.push(
      this.fb.group({
        id: [''],
        no_return: [''],
        with_return: [''],
        min_quantity: [''],
        segment: [segment],
        service: [service],
      })
    )
  }

  mostrarPricing(){
    this.layoutService.pricings({ company: this.actr.snapshot.params.id })
    .subscribe((response : any)=>{
      this.pricings = response
      this.company_price.patchValue(this.pricings);
    })
  }

  ViewPricings() {
    this.layoutService.pricings().subscribe((response : any) => {
      this.pricings = response.results;
        },
      error=>{
        for(let element in error.error){
          this.toastrService.error(error.error[element],element);
        }
      }
    )
  }

  listExecute(){
    this.layoutService.retieveUserInfo()
      .pipe(
        switchMap ( (data: any) => {
          return this.companiesService.listUsers(
            data.branch.company_id,
            { limit: 999, groups: 3 }
          )
        })
      )
      .subscribe((data: any) => this.executivesList = data.results )
  }

  get trader() {
    return this.companyForm.get('trader') as FormControl
  }

  get billing_contacts(){
    return this.companyForm.get('billing_contacts') as FormArray
  }

  selectTrader(id: number){
    this.trader.patchValue(this.executivesList.find(el => el.id == id));
  }

  addBillingInfo(){
    if(this.billing_contacts.controls.length < 3){
      this.billing_contacts.push(
        this.fb.group({
          name: ['', Validators.required],
          email: ['', [Validators.required, Validators.email]],
          phone: [''],
          address: [''],
        }));
    }
  }

  saveCompany(){
    this.loading = true;
    if (this.actr.snapshot.params.id) {
      let id = this.actr.snapshot.params.id;
      this.companiesService.editCompany(id, this.companyForm.value)
        .subscribe(
          (response: any) => {
            this.loading = false;
            this.toastrService.success('', 'Datos Cambiados correctamente');
            this.Router.navigate(['../'], { relativeTo: this.actr });
          },
          error => {
            this.loading = false;
            console.log(error);
            if (error.errMsg && error.status != 500) {
              for (var errorHead in error.errMsg) {
                this.toastrService.error(error.errMsg[errorHead], FIELDS[errorHead])
              }
            } else {
              console.log(error);
              this.toastrService.error("Algo ha salido mal");
            }
          }
        )
    } else {
      this.companiesService.createCompany(this.companyForm.value)
      .subscribe(
        (response : any )=>{
          this.loading = false;
          this.toastrService.success('Datos guardados' ,'',{ timeOut: 6000 });
          this.Router.navigate(['../../adm']);
        },
        error => { 
          this.loading = false;
          if (error.errMsg && error.status !=500){
            for (var errorHead in error.errMsg){
              this.toastrService.error(error.errMsg[errorHead],FIELDS[errorHead])
            }
          }else{
            console.log(error);
            this.toastrService.error("Algo ha salido mal");
          }
        } 
      )
    }
  }
}

