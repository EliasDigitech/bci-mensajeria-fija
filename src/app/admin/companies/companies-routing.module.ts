import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompaniesComponent } from './companies.component';
import { EditLiscompanyComponent } from './edit-liscompany/edit-liscompany.component';
import { CreateCompanyComponent } from './create-company/create-company.component';
import { ListUserComponent } from './list-user/list-user.component'
import { CreateBranchComponent } from './create-branch/create-branch.component'
import { CreateUserComponent } from './create-user/create-user.component'
import { FilesListComponent } from './files-list/files-list.component'
import { FichaComponent } from '../../layouts/shared/ficha/ficha.component';

import { CompanyResolver, BranchResolver, UserResolver } from './services/companies.resolve';
import { FileResolver } from '../../layouts/services/file.resolve';

const routes: Routes = [
  {
    path: '',
    component: CompaniesComponent,
  },
  {
    path: 'new',
    component: CreateCompanyComponent,
  },
  {
    path: 'files',
    component: FilesListComponent,
    data: {
      title: 'Listado de Fichas'
    },
  },
  {
    path: 'files/:id',
    component: FichaComponent,
    resolve: { file: FileResolver },
    data: { 
      group: 'YWRtaW5z',
      title: 'Ficha'
    }
  },
  {
    path: ':id',  // REVISAR GUARD O RESOLVER PARA SEGMENTO
    component: EditLiscompanyComponent,
    resolve: { company: CompanyResolver },
    data: {
      title: 'Detalles de empresa'
    },
  },
  {
    path: ':id/change',
    component: CreateCompanyComponent,
    resolve: { company: CompanyResolver },
    data: {
      title: 'Modificar empresa'
    },
  },
  {
    path: ':id/:branch_id',
    component: CreateBranchComponent,
    resolve: { branch: BranchResolver },
    data: {
      title: 'Sucursal'
    },
  },
  {
    path: ':id/:branch_id/users',
    component: ListUserComponent,
    resolve: { 
      branch: BranchResolver,
      company: CompanyResolver
    },
    data: {
      title: 'Listado de usuarios'
    },
  },
  {
    path: ':id/:branch_id/users/:user_id',
    component: CreateUserComponent,
    resolve: { user: UserResolver },
    data: {
      title: 'Usuario'
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    CompanyResolver,
    FileResolver,
    BranchResolver,
    UserResolver,
  ]
})
export class CompaniesRoutingModule { }
