import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable, EMPTY } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { SettingsService } from './settings.service';

@Injectable()
export class ServiceResolver implements Resolve<any> {
    constructor(
        private settingsService: SettingsService,
        private router: Router,
    ) { }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any> | Promise<any> | any {
        if (route.params['id'] === 'new'){
            return 'new';
        }
        return this.settingsService.retrieveService(route.params['id'])
            .pipe(
                catchError(err => {
                    this.router.navigate(['404']);
                    return EMPTY;
                })
            )
    }
}

@Injectable()
export class SegmentResolver implements Resolve<any> {
    constructor(
        private settingsService: SettingsService,
        private router: Router,
    ) { }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any> | Promise<any> | any {
        if (route.params['id'] === 'new') {
            return 'new';
        }
        return this.settingsService.retrieveSegment(route.params['id'])
            .pipe(
                catchError(err => {
                    this.router.navigate(['404']);
                    return EMPTY;
                })
            )
    }
}

@Injectable()
export class ProductResolver implements Resolve<any> {
    constructor(
        private settingsService: SettingsService,
        private router: Router,
    ) { }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any> | Promise<any> | any {
        if (route.params['id'] === 'new') {
            return 'new';
        }
        return this.settingsService.retrieveProduct(route.params['id'])
            .pipe(
                catchError(err => {
                    this.router.navigate(['404']);
                    return EMPTY;
                })
            )
    }
}

@Injectable()
export class TransportResolver implements Resolve<any> {
    constructor(
        private settingsService: SettingsService,
        private router: Router,
    ) { }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any> | Promise<any> | any {
        if (route.params['id'] === 'new') {
            return 'new';
        }
        return this.settingsService.retrieveTransport(route.params['id'])
            .pipe(
                catchError(err => {
                    this.router.navigate(['404']);
                    return EMPTY;
                })
            )
    }
}

@Injectable()
export class ReasonResolver implements Resolve<any> {
    constructor(
        private settingsService: SettingsService,
        private router: Router,
    ) { }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any> | Promise<any> | any {
        if (route.params['id'] === 'new') {
            return 'new';
        }
        return this.settingsService.retrieveReason(route.params['id'])
            .pipe(
                catchError(err => {
                    this.router.navigate(['404']);
                    return EMPTY;
                })
            )
    }
}