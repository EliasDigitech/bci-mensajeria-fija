import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment'
import { CreateCompany , ListUsers, CreateBranch } from "./../models";
import { XhrErrorHandlerService } from '../services/xhr-error-handler.service';
import { catchError } from 'rxjs/operators';

@Injectable()
export class AdmService {
  
  dtOptions: DataTables.Settings = {};

  ListUsers : ListUsers = new ListUsers;
  
  constructor(
    private http:HttpClient,
    private xhrErrorHandlerService: XhrErrorHandlerService
  ) { }


 // Company Files related

  public listAvailableFiles(params: any) {
    return this.http.get(environment.server_ip + 'company_files/', { params : params})
  }

  public updateFile(id:number, data:any){
    return this.http.patch(environment.server_ip + 'company_files/' + id + '/', data)
    .pipe(
      catchError(this.xhrErrorHandlerService.handleError)
    );
  }

  public listcompanyParams(consulta?: any) {
    return this.http.get(environment.server_ip + 'companies/', { params: consulta })
      .pipe(
        catchError(this.xhrErrorHandlerService.handleError)
      );
  }
}
