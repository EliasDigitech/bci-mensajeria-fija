import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment'
import { Observable } from 'rxjs';
import { Pricings } from '../models';
import { catchError } from 'rxjs/operators';

import { XhrErrorHandlerService } from './xhr-error-handler.service';
import { Services, Segments, Products } from '../models';

@Injectable()
export class SettingsService {

  constructor(
    private http: HttpClient,
    private xhrErrorHandlerService: XhrErrorHandlerService,
  ) { }
  
  // Services related 
  public listServices(params: any) {
    return this.http.get(environment.server_ip + 'settings/services/', { params: params } )
      .pipe(
        catchError(this.xhrErrorHandlerService.handleError)
      )
  }

  public retrieveService(id:Number) { 
    return this.http.get(environment.server_ip + 'settings/services/' + id + '/')
      .pipe(
        catchError(this.xhrErrorHandlerService.handleError)
      )
  }

  public createService(data: Services){
    return this.http.post(environment.server_ip + 'settings/services/', data)
      .pipe(
        catchError(this.xhrErrorHandlerService.handleError)
      )
  }

  public modifyService(id: Number, data: Services) {
    return this.http.put(environment.server_ip + 'settings/services/' + id + '/', data)
      .pipe(
        catchError(this.xhrErrorHandlerService.handleError)
      )
  }

  // Products related 
  
  public listProducts(params: any) {
    return this.http.get(environment.server_ip + 'settings/products/', { params: params })
      .pipe(
        catchError(this.xhrErrorHandlerService.handleError)
      )
  }

  public retrieveProduct(id: Number) {
    return this.http.get(environment.server_ip + 'settings/products/' + id + '/')
      .pipe(
        catchError(this.xhrErrorHandlerService.handleError)
      )
  }

  public createProduct(data: Products) {
    return this.http.post(environment.server_ip + 'settings/products/', data)
      .pipe(
        catchError(this.xhrErrorHandlerService.handleError)
      )
  }

  public modifyProduct(id: Number, data: Products) {
    return this.http.put(environment.server_ip + 'settings/products/' + id + '/', data)
      .pipe(
        catchError(this.xhrErrorHandlerService.handleError)
      )
  }

  // Segments related 
  
  public listSegments(params: any) {
    return this.http.get(environment.server_ip + 'settings/segments/', { params: params })
      .pipe(
        catchError(this.xhrErrorHandlerService.handleError)
      )
  }

  public retrieveSegment(id: Number) {
    return this.http.get(environment.server_ip + 'settings/segments/' + id + '/')
      .pipe(
        catchError(this.xhrErrorHandlerService.handleError)
      )
  }

  public createSegment(data: Segments) {
    return this.http.post(environment.server_ip + 'settings/segments/', data)
      .pipe(
        catchError(this.xhrErrorHandlerService.handleError)
      )
  }

  public modifySegment(id: Number, data: Segments) {
    return this.http.put(environment.server_ip + 'settings/segments/' + id + '/', data)
      .pipe(
        catchError(this.xhrErrorHandlerService.handleError)
      )
  }

  // Transports related

  public listTransports(params: any) {
    return this.http.get(environment.server_ip + 'settings/transports/', { params: params })
      .pipe(
        catchError(this.xhrErrorHandlerService.handleError)
      )
  }

  public retrieveTransport(id: Number) {
    return this.http.get(environment.server_ip + 'settings/transports/' + id + '/')
      .pipe(
        catchError(this.xhrErrorHandlerService.handleError)
      )
  }

  public createTransport(data: Segments) {
    return this.http.post(environment.server_ip + 'settings/transports/', data)
      .pipe(
        catchError(this.xhrErrorHandlerService.handleError)
      )
  }

  public modifyTransport(id: Number, data: Segments) {
    return this.http.put(environment.server_ip + 'settings/transports/' + id + '/', data)
      .pipe(
        catchError(this.xhrErrorHandlerService.handleError)
      )
  }

  // Reasons related
  public listReasons(params: any) {
    return this.http.get(environment.server_ip + 'settings/reasons/', { params: params })
      .pipe(
        catchError(this.xhrErrorHandlerService.handleError)
      )
  }

  public retrieveReason(id: Number) {
    return this.http.get(environment.server_ip + 'settings/reasons/' + id + '/')
      .pipe(
        catchError(this.xhrErrorHandlerService.handleError)
      )
  }

  public createReason(data: any) {
    return this.http.post(environment.server_ip + 'settings/reasons/', data)
      .pipe(
        catchError(this.xhrErrorHandlerService.handleError)
      )
  }

  public modifyReason(id: Number, data: any) {
    return this.http.put(environment.server_ip + 'settings/reasons/' + id + '/', data)
      .pipe(
        catchError(this.xhrErrorHandlerService.handleError)
      )
  }

  public deleteReason(id: Number) {
    return this.http.delete(environment.server_ip + 'settings/reasons/' + id + '/')
      .pipe(
        catchError(this.xhrErrorHandlerService.handleError)
      )
  }

}
