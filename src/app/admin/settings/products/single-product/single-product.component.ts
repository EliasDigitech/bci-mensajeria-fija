import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { MetaTitleService } from '../../../../services/meta-title.service';
import { SettingsService } from '../../../services/settings.service';

@Component({
  selector: 'app-single-product',
  templateUrl: './single-product.component.html',
  styleUrls: ['./single-product.component.scss']
})
export class SingleProductComponent implements OnInit, OnDestroy {

  private $destroy = new Subject<boolean>();
  private productId: Number;
  public isLoading: boolean = false;

  productForm = this.fb.group({
    name: ['', Validators.required],
    default: ['yes'],
    status: ['active', Validators.required],
  })

  constructor(
    private fb: FormBuilder,
    private settingsService: SettingsService,
    private route: ActivatedRoute,
    private toast: ToastrService,
    private router: Router,
    private metaTitleService: MetaTitleService,
  ) {
    this.metaTitleService.updateTitle();
      this.route.data
      .pipe(
        takeUntil(this.$destroy)
      )
      .subscribe(
        (data: any) => {
          if (data.product !== 'new'){
              this.productId = data.product.id;
              this.productForm.patchValue(data.product);
          }
        }
      )
  }

  ngOnInit() {
  }

  handleSave(){
    this.isLoading = false;
    this.toast.success("Producto guardado exitosamente");
    this.router.navigate(['../'], { relativeTo: this.route });
  }
  private handleErr(err){
    this.isLoading = false;
    // console.log(err);
  }
  saveOrCreate(){
    this.isLoading = true;
    if (this.productId){
      this.settingsService.modifyProduct(this.productId, this.productForm.value)
        .subscribe(
          () => this.handleSave(),
          err => this.handleErr(err)
        )
    }else{
      this.settingsService.createProduct(this.productForm.value)
        .subscribe(
          () => this.handleSave(),
          err => this.handleErr(err)
        )
      }
    }
  
  ngOnDestroy(){
    this.$destroy.next(true);
    this.$destroy.complete();
  }

}
