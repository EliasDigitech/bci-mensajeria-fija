import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SettingsService } from '../../../services/settings.service';
import { MetaTitleService } from '../../../../services/meta-title.service';

@Component({
  selector: 'app-transport-list',
  templateUrl: './transport-list.component.html',
  styleUrls: ['./transport-list.component.scss']
})
export class TransportListComponent implements OnInit, OnDestroy {
  private destroy$: Subject<boolean> = new Subject<boolean>();
  currentPage: Array<any> = [];

  constructor(
    private settingsService: SettingsService,
    private router: Router,
    private route: ActivatedRoute,
    private metaTitleService: MetaTitleService,
  ) {
    this.metaTitleService.updateTitle();
  }

  dtOptions: any = {
    dom: 'Bfrtip',
    serverSide: true,
    processing: true,
    language: {
      url: '../../../assets/languages/Spanish.json'
    },
    buttons: {
      buttons: [
        {
          text: 'Crear',
          key: '1',
          className: 'btn btn-primary btn-sm',
          action: (e, dt, node, config) => {
            this.router.navigate(['./new'], { relativeTo: this.route });
          }
        },
      ]
    },
    columns: [
      {
        data: 'plate',
      }, {
        data: 'status',
      },{
        data: 'id',
      },{
        data: 'id',
        orderable: false
      },
    ],
    ajax: (data: any, callback) => {
      let order = data.order[0];
      let filters: any = {
        search: data.search.value,
        offset: data.start,
        limit: data.length,
        ordering: (order.dir == 'desc' ? '-' : '') + data.columns[order.column].data,
      };
      this.settingsService.listTransports(filters)
      .pipe(
        takeUntil(this.destroy$)
      )
        .subscribe(
          (response: any) => {
            this.currentPage = response.results;
            callback({
              recordsTotal: response.count,
              recordsFiltered: response.count,
              data: []
            });
          },
          err => {
            console.log(err)
          }
        )
    },
  };

  ngOnInit(): void {
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

}
