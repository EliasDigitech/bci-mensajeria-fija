import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, combineLatest } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import { LayoutService } from '../../../../layouts/services/layout.service';
import { SettingsService } from '../../../services/settings.service';
import { MetaTitleService } from '../../../../services/meta-title.service';
import { Services } from '../../../models';

@Component({
  selector: 'app-single-segment',
  templateUrl: './single-segment.component.html',
  styleUrls: ['./single-segment.component.scss']
})
export class SingleSegmentComponent implements OnInit, OnDestroy {

  private $destroy = new Subject<boolean>();
  public services: Array<Services> = new Array<Services>();
  private serviceId: Number;
  public isLoading: boolean = false;

  segmentForm = this.fb.group({
    name: ['', Validators.required],
    height: ['', Validators.required],
    width: ['', Validators.required],
    depth: ['', Validators.required],
    min_weight: ['', Validators.required],
    max_weight: ['', Validators.required],
    company: [''],
    default: [''],
    segment_price: this.fb.array([]),
  })

  constructor(
    private fb: FormBuilder,
    private layoutService: LayoutService,
    private settingsService: SettingsService,
    private route: ActivatedRoute,
    private toast: ToastrService,
    private router: Router,
    private metaTitleService: MetaTitleService,
  ) {
    this.metaTitleService.updateTitle();
    let params = this.route.snapshot.queryParams;
    combineLatest(
      this.route.data,
      this.layoutService.services({ company_default: params.company, default: params.default == 'yes'?params.default : '' })
    )
      .pipe(
        takeUntil(this.$destroy)
      )
      .subscribe(
        (data: any) => {
          this.services = data[1].results;
          if (data[0].service === 'new') {
            for (let segment of this.services) {
              this.addSegmentPricingInfo(segment.id, null, true, params.company, params.default)
              this.segmentForm.patchValue({ company: params.company, default: params.default});
            }
          } else {
            this.serviceId = data[0].service.id;
            for (let price of data[0].service.segment_price) {
              this.addSegmentPricingInfo(price.segment, price.service, false, price.company, price.default)
            }
            this.segmentForm.patchValue(data[0].service);
          }
        }
      )
  }

  ngOnInit() {
  }

  get segment_price() {
    return this.segmentForm.get('segment_price') as FormArray
  }

  addSegmentPricingInfo(service: number, segment: number, disabled: boolean, company: number, isDefault: string) {
    this.segment_price.push(
      this.fb.group({
        id: [{ value: '', disabled: disabled }],
        no_return: ['', Validators.required],
        with_return: ['', Validators.required],
        service: [service],
        segment: [segment],
        company: [company],
        default: [isDefault],
      })
    )
  }

  getService(serviceId: Number) {
    return this.services.find(el => {
      return el.id == serviceId;
    })
  }

  handleSave() {
    this.isLoading = false;
    this.toast.success("Tramo guardado exitosamente");
    this.router.navigate(['../'], { relativeTo: this.route });
  }
  private handleErr(err) {
    this.isLoading = false;
    // console.log(err);
  }
  saveOrCreate() {
    this.isLoading = true;
    if (this.serviceId) {
      this.settingsService.modifySegment(this.serviceId, this.segmentForm.value)
        .subscribe(
          () => this.handleSave(),
          err => this.handleErr(err)
        )
    } else {
      this.settingsService.createSegment(this.segmentForm.value)
        .subscribe(
          () => this.handleSave(),
          err => this.handleErr(err)
        )
    }
  }

  ngOnDestroy() {
    this.$destroy.next(true);
    this.$destroy.complete();
  }
}
