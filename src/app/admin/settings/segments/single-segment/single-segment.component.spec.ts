import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleSegmentComponent } from './single-segment.component';

describe('SingleSegmentComponent', () => {
  let component: SingleSegmentComponent;
  let fixture: ComponentFixture<SingleSegmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleSegmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleSegmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
