import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, combineLatest } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import { LayoutService } from '../../../../layouts/services/layout.service';
import { SettingsService } from '../../../services/settings.service';
import { MetaTitleService } from '../../../../services/meta-title.service';
import { Segments } from '../../../models';

@Component({
  selector: 'app-single-service',
  templateUrl: './single-service.component.html',
  styleUrls: ['./single-service.component.scss']
})
export class SingleServiceComponent implements OnInit, OnDestroy {

  private $destroy = new Subject<boolean>();
  public segments: Array<Segments> = new Array<Segments>();
  private serviceId: Number;
  public isLoading: boolean = false;

  serviceForm = this.fb.group({
    name: ['', Validators.required],
    status: ['active', Validators.required],
    company: [''],
    default: [''],
    service_price : this.fb.array([]),
  })

  constructor(
    private fb: FormBuilder,
    private layoutService: LayoutService,
    private settingsService: SettingsService,
    private route: ActivatedRoute,
    private toast: ToastrService,
    private router: Router,
    private metaTitleService: MetaTitleService,
  ) {
    this.metaTitleService.updateTitle();
    let params = this.route.snapshot.queryParams
      combineLatest(
        this.route.data,
        this.layoutService.segments({ company_default: params.company, default: params.default == 'yes' ? params.default : ''  })
      )
      .pipe(
        takeUntil(this.$destroy)
      )
      .subscribe(
        (data: any) => {
          this.segments = data[1].results;
          if (data[0].service === 'new'){
            for (let segment of this.segments) {
              this.addServicePricingInfo(segment.id, null, true, params.default, params.company)
            }
            this.serviceForm.patchValue({ company: params.company, default: params.default})
          }else{
            this.serviceId = data[0].service.id;
            for (let price of data[0].service.service_price){
              this.addServicePricingInfo(price.segment, price.service, false, price.default, price.company)
            }
            this.serviceForm.patchValue(data[0].service);
          }
        }
      )
  }

  ngOnInit() {
  }

  get service_price() {
    return this.serviceForm.get('service_price') as FormArray
  }

  addServicePricingInfo(segment: number, service:number, disabled: boolean, isDefault: string, company: number) {
    this.service_price.push(
      this.fb.group({
        id: [{value: '', disabled: disabled}],
        no_return: ['', Validators.required],
        with_return: ['', Validators.required],
        default: [isDefault],
        company: [company],
        segment: [segment],
        service: [service],
      })
    )
  }

  getSegment(segmentId:Number){
    return this.segments.find( el => {
      return el.id == segmentId;
    })
  }
  
  handleSave(){
    this.isLoading = false;
    this.toast.success("Servicio guardado exitosamente");
    this.router.navigate(['../'], { relativeTo: this.route });
  }
  private handleErr(err){
    this.isLoading = false;
    // console.log(err);
  }
  saveOrCreate(){
    this.isLoading = true;
    if (this.serviceId){
      this.settingsService.modifyService(this.serviceId, this.serviceForm.value)
        .subscribe(
          () => this.handleSave(),
          err => this.handleErr(err)
        )
    }else{
      this.settingsService.createService(this.serviceForm.value)
        .subscribe(
          () => this.handleSave(),
          err => this.handleErr(err)
        )
      }
    }
  
  ngOnDestroy(){
    this.$destroy.next(true);
    this.$destroy.complete();
  }
}
