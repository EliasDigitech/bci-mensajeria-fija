import { Component, OnInit, ViewChild, ElementRef, Renderer2, AfterViewInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { Observable, BehaviorSubject } from 'rxjs';
import { distinctUntilChanged, debounceTime, flatMap, map, tap } from 'rxjs/operators';
import { isNull } from 'underscore';
import { AdmService } from '../../../services/adm.service';
import { DataTableDirective, } from 'angular-datatables';
import { MetaTitleService } from '../../../../services/meta-title.service';
import { SettingsService } from '../../../services/settings.service';

@Component({
  selector: 'app-services-list',
  templateUrl: './services-list.component.html',
  styleUrls: ['./services-list.component.scss']
})
export class ServicesListComponent implements OnInit, AfterViewInit {
  @ViewChild('servicesTable') companyFilesTable : ElementRef;
  @ViewChild(DataTableDirective, { static: true }) dtElement: DataTableDirective;
  public companies: Observable<any>;
  searchTerm: BehaviorSubject<string> = new BehaviorSubject<string>("");

  dtOptions: any = {
    dom: 'Bfrtip',
    serverSide: true,
    processing: true,
    language: {
      url: '../../../assets/languages/Spanish.json'
    },
    buttons: {
      buttons: [
        {
          text: 'Crear',
          key: '1',
          className: 'btn btn-primary btn-sm',
          action: (e, dt, node, config) => {
            this.router.navigate(['./new'], { relativeTo: this.route, queryParams: this.companiesFilters.value });
          }
        },
        // {
        //   // text: '<i class="far fa-file-excel"></i> Excel',
        //   text: 'Excel',
        //   key: '2',
        //   className: 'btn btn-success btn-sm text-white',
        //   action: (e, dt, node, config) => {
        //     alert('Button activated');
        //   }
        // },{
        //   // text: '<i class="far fa-file-pdf"></i> PDF', 
        //   text: 'PDF', 
        //   key: '2',
        //   className: 'btn btn-danger btn-sm text-white',
        //   action: (e, dt, node, config) => {
        //     alert('Button activated');
        //   }
        // },
      ]
    },
    columns: [
      {
        title: 'Nombre',
        data: 'name',
      }, {
        title: 'Estado',
        data: 'status',
        render: function (data: any, type: any, full: any) {
          switch(data){
            case "active":
              return "Activo";
              break;
            case "disabled":
              return "Desactivado";
              break;
          }
        }
      }, {
        title: '',
        data: 'id',
        orderable: false,
        className: 'text-center',
        render: function (data: any, type: any, full: any) {
          return `<button type="button" class="btn btn-info btn-sm" view-file-id="${data}">
                    <i class="fas fa-eye" view-file-id="${data}"></i>
                  </button>`;
        }
      },
    ],
    ajax: (data: any, callback) => {
      let order = data.order[0];
      let filters: any = {
        search: data.search.value,
        offset: data.start,
        limit: data.length,
        ordering: (order.dir == 'desc' ? '-' : '') + data.columns[order.column].data,
        ...this.companiesFilters.value
      };
      this.settingsService.listServices(filters)
        .subscribe(
          (response: any) => {
            callback({
              recordsTotal: response.count,
              recordsFiltered: response.count,
              data: response.results
            });
          },
          err => {
            console.log(err)
          }
        )
    },
  };

  companiesFilters = this.fb.group({
    company: [''],
    default: ['yes']
  })

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private settingsService: SettingsService,
    private renderer: Renderer2,
    private admService: AdmService,
    private fb: FormBuilder,
    private metaTitleService: MetaTitleService,
  ) {
    this.metaTitleService.updateTitle();
  }

  ngOnInit() {
    this.companies = this.searchTerm
      .pipe(
        distinctUntilChanged(),
        debounceTime(400),
        flatMap(
          term => {
            return this.admService.listcompanyParams({ search: term })
              .pipe(
                map(
                  (res: any) => {
                    return res.results;
                  }
                )
              )
          }
        )
      )
    this.companiesFilters.controls['company'].valueChanges
      .pipe(
        tap(
          val => {
            if(isNull(val)){
              this.companiesFilters.patchValue({ company: '' })
            }
          }
        )
      )
      .subscribe(
        val => {
          if(isNull(val)){
            this.companiesFilters.patchValue({ default: 'yes' })
          }else{
            this.companiesFilters.patchValue({ default: 'no' })
          }
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.ajax.reload(() => { }, false);
          });
        }
      )
  }

  ngAfterViewInit(): void {
    this.renderer.listen(this.companyFilesTable.nativeElement, 'click', (event) => {
      if (event.target.hasAttribute("view-file-id")) {
        this.router.navigate([event.target.getAttribute("view-file-id")], { relativeTo: this.route, queryParams: this.companiesFilters.value });
      }
    });
  }

  search(search: string, item: any): boolean {
    return true;
  }

}
