import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsRoutingModule } from './settings-routing.module';
import { LayoutsModule } from '../../layouts/layouts.module';
import { CurrencyMaskModule } from "ng2-currency-mask";

import { ServicesListComponent } from './services/services-list/services-list.component';
import { SingleServiceComponent } from './services/single-service/single-service.component';

import { SettingsService } from '../services/settings.service';
import { SegmentsListComponent } from './segments/segments-list/segments-list.component';
import { SingleSegmentComponent } from './segments/single-segment/single-segment.component';
import { ProductListComponent } from './products/product-list/product-list.component';
import { SingleProductComponent } from './products/single-product/single-product.component';
import { TransportListComponent } from './transports/transport-list/transport-list.component';
import { SingleTransportComponent } from './transports/single-transport/single-transport.component';
import { ReasonsListComponent } from './failed-reasons/reasons-list/reasons-list.component';
import { SingleReasonComponent } from './failed-reasons/single-reason/single-reason.component';

@NgModule({
  declarations: [
    ServicesListComponent,
    SingleServiceComponent,
    SegmentsListComponent,
    SingleSegmentComponent,
    ProductListComponent,
    SingleProductComponent,
    TransportListComponent,
    SingleTransportComponent,
    ReasonsListComponent,
    SingleReasonComponent,
  ],
  imports: [
    CommonModule,
    SettingsRoutingModule,
    CurrencyMaskModule,
    LayoutsModule
  ],
  providers: [
    SettingsService,
  ]
})
export class SettingsModule { }
