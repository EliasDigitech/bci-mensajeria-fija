import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { BillingService } from '../../services/billing.service';
import { AdmService } from '../../services/adm.service';
import { Subject, Observable } from 'rxjs';
import { takeUntil, map } from 'rxjs/operators';
import { DataTableDirective } from 'angular-datatables';
import { MetaTitleService } from '../../../services/meta-title.service';

@Component({
  selector: 'app-pre',
  templateUrl: './pre.component.html',
  styleUrls: ['./pre.component.scss']
})
export class PreComponent implements OnInit, OnDestroy {
@ViewChild(DataTableDirective, { static: true }) dtElement: DataTableDirective;

  destroy$: Subject<boolean> = new Subject<boolean>();
  billingInfo: Observable<any>;
  workOrders: any;
  filtersForm = this.fb.group({
    company: ['1'],
    created_at__gte: [''],
    created_at__lte: ['']
  })
  companies: Observable<any>;
  // companies : Array<any>;

  constructor(
    private fb: FormBuilder,
    private billingService: BillingService,
    private admService: AdmService,
    private metaTitleService: MetaTitleService,
    ) {
      this.metaTitleService.updateTitle();
      this.companies = this.admService.listcompanyParams({ limit : 999 })
      .pipe(
        map( (res:any) => {
          for(let b in res.results){
            res.results[b].name = res.results[b].name[0].toUpperCase() + res.results[b].name.slice(1);
             }
          console.log(res.results)
          res.results = res.results.sort(function (a, b) {
            if (a.name > b.name) {
              return 1;
            }
            if (a.name < b.name) {
              return -1;
            }
            // a must be equal to b
            return 0;
          });
          return res.results
        })
        )
    // this.admService.listcompanyParams({ limit: 999 })
    //   .subscribe(
    //     (res: any) => {
    //       this.companies = res.results;
    //     }
    //   )
    }
  
  
  dtOptions: any = {
    dom: 'frtip',
    searching: false,
    serverSide: true,
    processing: true,
    language: {
      url: '../../../assets/languages/Spanish.json'
    },
    columns: [
      {
        title: 'N°',
        data: 'barcode'
      },
      {
        title: 'Fecha emisión',
        data: 'created_at',
        render: function (data: any, type: any, full: any) {
          if (data) {
            return data[0].toUpperCase() + data.slice(1);
          } else {
            return data;
          }
        }
      },
      {
        title: 'Nombre Emisor',
        data: 'sender_full_name',
        render: function (data: any, type: any, full: any) {
          if (data) {
            return data[0].toUpperCase() + data.slice(1);
          } else {
            return data;
          }
        }
      },
      {
        title: 'Dirección origen',
        data: 'sender_address'
      },
      {
        title: 'Nombre receptor',
        data: 'receiver_full_name'
      },
      {
        title: 'Dirección destino',
        data: 'receiver_address'
      },
      {
        title: 'Último estado',
        data: 'current_step.name'
      },
    ],
    ajax: (data: any, callback) => {
      let consulta: any = {
        offset: data.start,
        limit: data.length,
        excluded_status__in :'0',
        ...this.filtersForm.value
      };

      this.billingService.getWorkOrders(consulta)
        .pipe(
          takeUntil(this.destroy$)
        )
        .subscribe(
          (response: any) => {
            callback({
              recordsTotal: response.count,
              recordsFiltered: response.count,
              data: response.results
            });
          },
          error => {
            // console.log(error);
          }
        )
    },
  };

  ngOnInit() {
    this.seachPre()
  }

  seachPre(){
    this.billingInfo = this.billingService.getBillingInfo(this.filtersForm.value)
      .pipe(
        map((res: any) => res[0].segments)
      )
    if (this.dtElement.dtInstance){
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.ajax.reload();
      }); 
    }
  }

  downloadPre(){
    let params = {...this.filtersForm.value};
    params.type = "excel";
    this.billingService.getBillingInfo(params, true)
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(
        res => {
          const now = new Date();
          let anchor = document.createElement('a');
          anchor.download = "Empresas " + now.toLocaleString('es-CL') + ".xlsx";
          anchor.href = window.URL.createObjectURL(res);
          anchor.dataset.downloadurl = ['text/plain', anchor.download, anchor.href].join(':');
          anchor.click();
        }
      )
  }

  ngOnDestroy(){
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }
}