import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AdminRoutingModule } from './admin-routing.module';
import { LayoutsModule } from '../layouts/layouts.module';
import { AdminComponent } from './admin.component';
import { NgxMaskModule, IConfig } from 'ngx-mask'
import { JwtInterceptor } from '@auth0/angular-jwt';
import { AdmService } from './services/adm.service';
import { BillingService } from './services/billing.service';
import { PreComponent } from './billing/pre/pre.component';
import { NgxSelectModule } from 'ngx-select-ex';

@NgModule({
  declarations: [AdminComponent, PreComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    LayoutsModule,
    NgxSelectModule,
  ],
  providers:[
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
    AdmService,
    BillingService,
  ]
})
export class AdminModule { }
