import { Component, OnInit, Input, ViewChild, ElementRef, NgZone, OnDestroy } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { MapsAPILoader } from '@agm/core';
import { WoService } from '../../../layouts/services/wo-service.service';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-fix-address',
  templateUrl: './fix-address.component.html',
  styleUrls: ['./fix-address.component.scss']
})
export class FixAddressComponent implements OnInit, OnDestroy {
  @Input() wo: any;
  @ViewChild("senderAddressSearch") public senderAddressSearch: ElementRef;
  @ViewChild("receiverAddressSearch") public receiverAddressSearch: ElementRef;
  private sub: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    public modal: NgbActiveModal,
    private ngZone: NgZone,
    private mapsAPILoader: MapsAPILoader,
    private woService: WoService,
    private toastrService: ToastrService,
  ) { }
  
  updateForm = this.formBuilder.group({
    sender_address: ['', Validators.required],
    sender_comune: ['', Validators.required],
    sender_region: ['', Validators.required],
    sender_lat: ['', Validators.required],
    sender_lng: ['', Validators.required],
    sender_address_number: [''],
    receiver_address: ['', Validators.required],
    receiver_comune: ['', Validators.required],
    receiver_region: ['', Validators.required],
    receiver_lat: ['', Validators.required],
    receiver_lng: ['', Validators.required],
    receiver_address_number: ['']
  })
  ngOnInit() {
    switch (this.wo.invalid_type){
      case "receiver":
        this.updateForm.controls["sender_address"].disable();
        this.updateForm.controls["sender_comune"].disable();
        this.updateForm.controls["sender_region"].disable();
        this.updateForm.controls["sender_lat"].disable();
        this.updateForm.controls["sender_lng"].disable();
        this.updateForm.controls["sender_address_number"].disable();
        this.mapsAPILoader.load().then(() => {
          let autocomplete = new google.maps.places.Autocomplete(
            this.receiverAddressSearch.nativeElement,
            {
              types: ["address"],
              componentRestrictions: { country: "cl" }
            }
          );
          autocomplete.addListener("place_changed", () => {
            this.ngZone.run(() => {
              let place: google.maps.places.PlaceResult = autocomplete.getPlace();
              if (place.geometry === undefined || place.geometry === null) {
                return;
              }
              let components = place.address_components;
              let addressValues = {
                receiver_address: place.name,
                receiver_region: components.find(el => el.types.includes("administrative_area_level_1")).long_name,
                receiver_comune: components.find(el => el.types.includes("administrative_area_level_3")).long_name,
                receiver_lat: Math.round(place.geometry.location.lat() * 10000000) / 10000000,
                receiver_lng: Math.round(place.geometry.location.lng() * 10000000) / 10000000,
              }
              this.updateForm.patchValue(addressValues);
            })
          });
        })
        break;

      case "sender":
        this.updateForm.controls["receiver_address"].disable();
        this.updateForm.controls["receiver_comune"].disable();
        this.updateForm.controls["receiver_region"].disable();
        this.updateForm.controls["receiver_lat"].disable();
        this.updateForm.controls["receiver_lng"].disable();
        this.updateForm.controls["receiver_address_number"].disable();
        this.mapsAPILoader.load().then(() => {
          let autocomplete = new google.maps.places.Autocomplete(
            this.senderAddressSearch.nativeElement,
            {
              types: ["address"],
              componentRestrictions: { country: "cl" }
            }
          );
          autocomplete.addListener("place_changed", () => {
            this.ngZone.run(() => {
              let place: google.maps.places.PlaceResult = autocomplete.getPlace();
              if (place.geometry === undefined || place.geometry === null) {
                return;
              }
              let components = place.address_components;
              let addressValues = {
                // sender_address_number: components.find(el => el.types.includes("street_number")).long_name,
                sender_address: place.name,
                sender_region: components.find(el => el.types.includes("administrative_area_level_1")).long_name,
                sender_comune: components.find(el => el.types.includes("administrative_area_level_3")).long_name,
                sender_lat: Math.round(place.geometry.location.lat() * 10000000) / 10000000,
                sender_lng: Math.round(place.geometry.location.lng() * 10000000) / 10000000,
              }
              this.updateForm.patchValue(addressValues);
            })
          });
        })
        break;
    }
  }

  fixAddress(){
    this.sub = this.woService.patchOT(this.wo.id, this.updateForm.value)
    .subscribe(
      res => {
        this.modal.close(res)
      },
      (error: any) => {
        if (error.status === 500) {
          this.toastrService.error("Ha ocurrido un problema");
        } else {
          for (let errorHead in error.errMsg) {
            this.toastrService.error(error.errMsg[errorHead]);
          }
        }
      }
    )
    
  }

  ngOnDestroy(){
    this.sub.unsubscribe();
  }

}
