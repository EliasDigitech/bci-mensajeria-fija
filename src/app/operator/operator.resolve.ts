import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable, EMPTY } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ServiceOperatorService } from './service/service-operator.service';

@Injectable()
export class OperatorResolver implements Resolve<any> {
    constructor(
        private settingsService: ServiceOperatorService,
        private router: Router,
    ) { }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any> | Promise<any> | any {
        if (route.params['id'] === 'new'){
            return 'new';
        }
        return this.settingsService.wiewListOtService(route.params['id'])
            .pipe(
                catchError(err => {
                    this.router.navigate(['404']);
                    return EMPTY;
                })
            )
    }
}