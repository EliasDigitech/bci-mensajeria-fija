import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { ServiceOperatorService } from '../service/service-operator.service';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';
import { takeUntil, retryWhen, delay } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { MetaTitleService } from '../../services/meta-title.service';

@Component({
  selector: 'app-importdatafalla',
  templateUrl: './importdatafalla.component.html',
  styleUrls: ['./importdatafalla.component.scss']
})
export class ImportdatafallaComponent implements OnInit, OnDestroy {

  @ViewChild('woFile') woFile: ElementRef;
  public isLoading: boolean = false;
  public uploadResult: any = { type: 'waiting', human: 'En Espera'};
  public last_upload = [];
  public uploadSock: any;
  destroy$ = new Subject<boolean>();

  FIELDS = {
    'sender_full_name': 'Remitente',
    'sender_rut': 'RUT Remitente',
    'sender_email': 'Email Remitente',
    'sender_phone': 'Teléfono Remitente',
    'sender_address': 'Dirección Remitente',
    'sender_address_number': 'Dpto/Oficina Remitente',
    'sender_extra_info': 'Comentarios',
    'product_description': 'Descripción de producto',
    'receiver_full_name': 'Destinatario',
    'receiver_rut': 'RUT Destinatario',
    'receiver_email': 'Email Destinatario',
    'receiver_phone': 'Teléfono Destinatario',
    'receiver_address': 'Dirección Destinatario',
    'receiver_address_number': 'Dpto/Oficina Destinatario',
    'has_return': 'Retorno',
    'height': 'Alto',
    'width': 'Ancho',
    'depth': 'Largo',
    'weight': 'Peso',
    'product': 'Producto',
    'service': 'Servicio',
    'segment': 'Tramo',
    'extra_fields': 'Campos Extras',
    'sender_lat' : 'Latitud Remitente',
    'sender_lng' : 'Longitud Remitente',
    'receiver_lat' : 'Latitud Destinatario',
    'receiver_lng' : 'Longitud Destinatario'
  }
  arrayValid = true

  constructor(
    private ServiceOperatorService: ServiceOperatorService,
    private toastrService: ToastrService,
        private metaTitleService: MetaTitleService,
  ) {
    this.metaTitleService.updateTitle();
  }

  ngOnInit() {
    this.uploadSock = this.ServiceOperatorService.connectSocket('fallab_sync')
    this.uploadSock
      .pipe(
        retryWhen(err => err.pipe(delay(5000))),
        takeUntil(this.destroy$)
      )
      .subscribe(
        event => {
          this.isLoading = false;
          let data = JSON.parse(event.data)
          console.log(data)
          this.last_upload = data.map(el => el.barcode);
          this.uploadResult = {
            type: 'success',
            human: 'Exitoso',
            details: {
              qty: data.length,
              barcodes: this.last_upload.toString().replace(/,/g, ", ")
            }
          }
        })
  }

  beginUpload(){
    // console.log(this.woFile)
    this.uploadResult = { type: 'loading', human:'Cargando' };
    this.isLoading = true;
    let fd = new FormData()
    let native = this.woFile.nativeElement
    fd.append("wo_file", native.files[0])
    this.ServiceOperatorService.csvFalabellaUpload(fd)
      .subscribe(
        (res: any) => {
          // console.log(res)
          // this.isLoading = false;
          // this.last_upload = res.map(el => el.barcode);
          // this.uploadResult = {
          //   type: 'success',
          //   human: 'Exitoso',
          //   details: {
          //     qty: res.length,
          //     barcodes: this.last_upload.toString().replace(/,/g, ", ")
          //   }
          // }
          this.uploadResult = {
            type: 'processing',
            human: 'Procesando Archivo',
            // details: {
            //   qty: res.length,
            //   barcodes: this.last_upload.toString().replace(/,/g, ", ")
            // }
          }
        },
        (err: any) => {
          console.log(err)
          // this.toastrService.error('Algo salió mal', 'Error');
          // if (Array.isArray(err.errMsg)==false)
            // this.arrayValid= false
          this.isLoading = false;
          if (err.status == 400){
            let errors = [];
            if (Array.isArray(err.errMsg)){
              for (let i=0; i<err.errMsg.length; i++){
                let currentMsg = [];
                if (Object.keys(err.errMsg[i]).length == 0){
                  continue;
                }
                for(let key in err.errMsg[i]){
                  currentMsg.push({
                    field: this.FIELDS[key],
                    detail: err.errMsg[i][key]
                  })
                }
                errors.push({
                  row: i + 1,
                  messages: currentMsg
                })
                this.uploadResult = {
                  type : 'error',
                  human: 'Error',
                  details: errors
                }
              }
            }else{
              this.arrayValid= false
              this.uploadResult = {
                type: 'error',
                human: 'Formato no válido',
              }
            }
          }else{
            this.uploadResult = {
              type: 'error',
              human: 'Ha ocurrido un problema al cargar el archivo'
            }
          }
        }
      )
  }

  printAll(){
    window.open(environment.server_ip + 'wo/print/?barcodes=' + this.last_upload + '&ordering=receiver_comune');
  }

  ngOnDestroy(){
    this.uploadSock.complete();
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
}
