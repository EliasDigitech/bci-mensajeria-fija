import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportdatafallaComponent } from './importdatafalla.component';

describe('ImportdatafallaComponent', () => {
  let component: ImportdatafallaComponent;
  let fixture: ComponentFixture<ImportdatafallaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportdatafallaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportdatafallaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
