import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as qz from 'qz-tray';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';
import { MetaTitleService } from '../../services/meta-title.service';
@Component({
  selector: 'app-print-falabella',
  templateUrl: './print-falabella.component.html',
  styleUrls: ['./print-falabella.component.scss']
})
export class PrintFalabellaComponent implements OnInit, OnDestroy {

  printers: Array<String>;
  currentPrinter: string;
  currentSOC: string;
  private printerConf: any

  constructor(
    private http: HttpClient,
    private toastr: ToastrService,
    private metaTitleService: MetaTitleService,
  ) {
    this.metaTitleService.updateTitle();
  }

  ngOnInit(): void {
    //qz.api.setPromiseType(function promise(resolver) { return new Promise(resolver); });
    qz.security.setCertificatePromise((resolve, reject) => {
      resolve(
        '-----BEGIN CERTIFICATE-----\n'+
        'MIIEDzCCAvegAwIBAgIUHWasy0eFa8BirfIrLJhxKnuep78wDQYJKoZIhvcNAQEL\n' +
        'BQAwgZUxCzAJBgNVBAYTAkNMMREwDwYDVQQIDAhTYW50aWFnbzERMA8GA1UEBwwI\n' +
        'U2FudGlhZ28xFjAUBgNVBAoMDUNvbmV4eGlvbiBTcEExCzAJBgNVBAsMAlRJMRcw\n' +
        'FQYDVQQDDA4qLmNvbmV4eGlvbi5jbDEiMCAGCSqGSIb3DQEJARYTc29wb3J0ZUBk\n' +
        'aWdpdGVjaC5jbDAgFw0yMTAxMDQxOTU2MzhaGA8yMDUyMDYyOTE5NTYzOFowgZUx\n' +
        'CzAJBgNVBAYTAkNMMREwDwYDVQQIDAhTYW50aWFnbzERMA8GA1UEBwwIU2FudGlh\n' +
        'Z28xFjAUBgNVBAoMDUNvbmV4eGlvbiBTcEExCzAJBgNVBAsMAlRJMRcwFQYDVQQD\n' +
        'DA4qLmNvbmV4eGlvbi5jbDEiMCAGCSqGSIb3DQEJARYTc29wb3J0ZUBkaWdpdGVj\n' +
        'aC5jbDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMciw2jBuNL93nB8\n' +
        'LAySPeZPoNFwD3myL+Oi3tSSFU4iEUp8wnoUaE+BzP6rzUktcPUIcdjAhEhzWEqW\n' +
        '8vH2ztkMcJNQsTZUmA20MJquy/iN5DXaWK4zxKLda5R0NmlVDXHI+ut0IqhTuK1H\n' +
        '3EPSYcWH+JqFGjz6SNRjrqGxS3o10f1Crk0Hxaa22i9t9VSglMcSB5ZEiwanca7u\n' +
        'lH5o30YQ1ndu+WLhwCHcVpiZfVwdhdk/MQwC8zZQeWhbwm17h6qBZCv2xWOHNcYT\n' +
        'bRR7VMi9qz340V8JHCh51Mj++cP/ldGt5nQ5g62FrIN4s+ZNz6fiohb+gNm+Xlg6\n' +
        'xytp/p0CAwEAAaNTMFEwHQYDVR0OBBYEFJSR6QCuTT8v0NcbYer8rtt+dM5PMB8G\n' +
        'A1UdIwQYMBaAFJSR6QCuTT8v0NcbYer8rtt+dM5PMA8GA1UdEwEB/wQFMAMBAf8w\n' +
        'DQYJKoZIhvcNAQELBQADggEBABfR/2iBReZcWNGfkgxfzVFDytzE2BeIdM4UG8+M\n' +
        'jP80eeLSEjQWzZ8dgsJ+JL+6wUoaJ07kMpT5An97A5eixlaL9ppdXcXX6ODa47vY\n' +
        'PNXvHAOBtWxruMkqJIbv5zpldNYRx4c+1m0U9W1U9CI4p16ypbnyhXs3dgEsmKGw\n' +
        'PAmmr0r+/ML9iffU6oT3AmyvO+aDHUFqdphohB7oK9+0hcR3x07AtlDyglHbRYLW\n' +
        'ZWRfKKENKE30NlmkomsF6NI/MWuAFHgSUp2TphuFeIrvk/1/dTwdrlTYXFxwv6A6\n' +
        'fuQIjlytFxoZoc5+DffMKK26mZeR6qkzT/XBDZKYZHDtykQ=\n' +
        '-----END CERTIFICATE-----\n'
      )
    })

    //qz.api.setSha256Type(data => sha256(data));
    qz.security.setSignatureAlgorithm('SHA512');
    qz.security.setSignaturePromise(toSign => {
      return (resolve, reject) => {
        this.http.get(
          environment.server_ip + 'wo/print/sign/',
          { 
            params: { request: toSign },
            headers: {
              'Content-Type': 'text/plain'
            },
            responseType: 'text',
          }
        ).subscribe(
          res => {
            resolve(res)
          },
          err => {
            reject(err);
          }
        )
      }
    })
    qz.websocket.connect()
    .then(() => {
      qz.printers.find().then( data =>  {
          this.printers = data;
        }
      )
    })
    .catch( (err) => {
      console.log(err)
      this.toastr.error("QZ Tray no instalado")
    });
  }

  doPrint(){
    let soc = this.currentSOC;
    this.currentSOC = "";
    if (!this.printerConf){
      this.toastr.error("Impresora no seleccionada");
    }
    this.http.get(
      environment.server_ip + 'wo/print/',
      { params: { soc: soc }, responseType: 'blob' }
    ).subscribe(
      res => {

        let vm = this;
        // let data = [
        //   {
        //     type: 'pixel',
        //     format: 'pdf',
        //     flavor: 'file',
        //     data: window.URL.createObjectURL(res)
        //   }
        // ];
        var reader = new FileReader();
        reader.readAsDataURL(res);
        reader.onloadend = function () {
          var base64data: any = reader.result;
          console.log(base64data);
          let data = [
            {
              type: 'pixel',
              format: 'pdf',
              flavor: 'base64',
              data: base64data.split(";base64,")[1]
            },
          ]
          qz.print(vm.printerConf, data).catch(function (e) {
            console.log(e)
            vm.toastr.error('No es posible imprimir')
          });
        }
      },
      err => {
        this.currentSOC = "";
        this.toastr.error('No se pebe recuperar la etiqueta');
      }
    )
  }

  changePrinter(){
    this.printerConf = qz.configs.create(this.currentPrinter, {
      size: { width: 90, height: 100 },
      units: 'mm',
    });
  }

  ngOnDestroy(){
    qz.websocket.disconnect();
  }
}
