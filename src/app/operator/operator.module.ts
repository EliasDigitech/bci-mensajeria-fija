import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OperatorRoutingModule} from './operator-routing';
import { OperatorComponent } from './operator.component';
import { LayoutsModule } from '../layouts/layouts.module'; 
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { DashboardOperatorComponent } from "./dashboard-operator/dashboard-operator.component";
import { EditOtComponent } from './edit-ot/edit-ot.component';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { AssignComponent } from './dashboard-operator/modals/assign/assign.component';
import { UpdateComponent } from './dashboard-operator/modals/update/update.component';
import { CurrencyMaskModule } from "ng2-currency-mask"; 
// import { UsersModule } from "../users/users.module";
import { ImportdatafallaComponent } from '../operator/importdatafalla/importdatafalla.component';
import { CommonsModule } from '../commons/commons.module';
import { AssignationReportComponent } from './assignation-report/generate/assignation-report.component';
import { RoutesComponent } from './routes/routes.component';
import { FixAddressComponent } from './routes/fix-address/fix-address.component';
import { ARListComponent } from './assignation-report/list/list.component';
import { PrintFalabellaComponent } from './print-falabella/print-falabella.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';


export const options: Partial<IConfig> | (() => Partial<IConfig>) = {}

@NgModule({
  declarations: [ 
    OperatorComponent,
    DashboardOperatorComponent,
    EditOtComponent,
    AssignComponent,
    UpdateComponent,
    ImportdatafallaComponent,
    AssignationReportComponent,
    RoutesComponent,
    FixAddressComponent,
    ARListComponent,
    PrintFalabellaComponent,
  ],
  imports: [
    // UsersModule,
    CurrencyMaskModule,
    CommonModule,
    OperatorRoutingModule,
    LayoutsModule,
    FormsModule,
    ReactiveFormsModule,
    AutocompleteLibModule,
    NgxMaskModule.forRoot(options),
    CommonsModule,
    NgxDatatableModule,
  ],
  entryComponents: [
    AssignComponent,
    UpdateComponent,
    FixAddressComponent,
  ]
})
export class OperatorModule { }
