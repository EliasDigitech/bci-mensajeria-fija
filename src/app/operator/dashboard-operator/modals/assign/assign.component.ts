import { Component, OnInit, ViewChild, OnDestroy, SecurityContext } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ServiceOperatorService } from "../../../service/service-operator.service";
import { ToastrService } from "ngx-toastr";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DataTableDirective } from 'angular-datatables';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { takeUntil, finalize, debounceTime, flatMap, distinctUntilChanged, map ,filter} from 'rxjs/operators';
import { DomSanitizer } from '@angular/platform-browser'
import { LayoutService } from '../../../../layouts/services/layout.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-assign',
  templateUrl: './assign.component.html',
  styleUrls: ['./assign.component.scss']
})
export class AssignComponent implements OnInit, OnDestroy {
  @ViewChild(DataTableDirective, { static: true }) asignTable: DataTableDirective;

  searchTerm: BehaviorSubject<string> = new BehaviorSubject<string>("");
  searchTermPeoneta: BehaviorSubject<string> = new BehaviorSubject<string>("");
  searchTermPatente: BehaviorSubject<string> = new BehaviorSubject<string>("");
  transporters: Observable<any>;
  peoneta: Observable<any>;
  private currentCompany: number;

  asigForm = this.formBuilder.group({
    transporter: ['', Validators.required],
    barcode: ['', Validators.required],
    transport: ['', Validators.required],
    transporter_helper: [''],
  })
  asigJson = []
  listBiker = []
  tipoVehiculo 
  lisPatente;
  inputPeoneta = true;


  dtOptionsTableModalAsig: any = {
    searching: false,
    data: this.asigJson,
    columns: [
      {
        title: 'OT',
        data: 'barcode'
      }, {
        title: 'Conductor',
        data: 'transporter'
      }, {
        title: '',
        data: 'status',
        render: function (data: any, type: any, full: any) {
          switch (data) {
            case "Cargando":
              return `<label class="text-warning">${data}</label>`;
              break;
            case "Ok":
              return `<label class="text-success">${data}</label>`
              break;
            default:
              return `<label class="text-danger">${data}</label>`;
          }
        }
      },
    ],
    language: {
      url: '../../../assets/languages/Spanish.json'
    }
  };
  dtTrigger: Subject<boolean> = new Subject<boolean>();
  destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private formBuilder: FormBuilder,
    private serviceOperatorService: ServiceOperatorService,
    private layoutService: LayoutService,
    private toastrService: ToastrService,
    public activeModal: NgbActiveModal,
    private domSanitizer: DomSanitizer,
    private route: ActivatedRoute,
    private router: Router,
    private modalService: NgbModal,
  ) { }

  ngOnInit() {

  this.searchTermPatente.pipe(
  debounceTime(500)
  ,distinctUntilChanged())
  .subscribe((str) =>{ 
    console.log(str)
    this.layoutService.getPatente({ search: str}).subscribe((res:any)=>{
    this.lisPatente = res.results
    console.log(res.results )
    })
  });


    // this.layoutService.getPatente(this.searchTerm).subscribe((res:any)=>{
    //   console.log(res.results);
    //   this.lisPatente = res.results
    // })

    this.layoutService.retieveUserInfo()
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(
        user => {
          this.currentCompany = user.branch.company_id;
        }
      )


      this.transporters = this.searchTerm
      .pipe(
        distinctUntilChanged(),
        debounceTime(400),
        flatMap(
          term => {
            return this.layoutService.getUsers(this.currentCompany, { groups: 6, status: 'active', ordering: 'first_name', search: term, limit: 20 })
              .pipe(
                map(
                  (res: any) => {
                    this.listBiker = res.results
                    return res.results;
                  }
                )
              )
          }
        )
      )
    

    this.peoneta = this.searchTermPeoneta
      .pipe(
        distinctUntilChanged(),
        debounceTime(400),
        flatMap(
          term => {
            return this.layoutService.getUsers(this.currentCompany, { groups: 6, status: 'active', ordering: 'first_name', search: term, limit: 20 })
              .pipe(
                map(
                  (res: any) => {
                    return res.results;
                  }
                )
              )
          }
        )
      )

  }

  saveAsig() {
    if (this.asigForm.valid) {
      let currentAsignation = {
        "transporter": this.listBiker.find(el => el.id == this.asigForm.value.transporter).name,
        "barcode": this.domSanitizer.sanitize(SecurityContext.HTML, this.asigForm.value.barcode),
        "status": "Cargando",
      }
      this.asigJson.push(currentAsignation);
      let current_data = {...this.asigForm.value};
      current_data.barcode = current_data.barcode.toUpperCase();
      this.doReload();
      this.asigForm.controls['barcode'].reset();

      this.serviceOperatorService.saveDashboardMdalServ(current_data)
      .pipe(
        finalize( () => {
          this.doReload();
        }),
        takeUntil(this.destroy$),
      )
        .subscribe((response: any) => {
          console.log(response)
          currentAsignation.status = "Ok"
        },
          error => {
            console.log(error)
            if (error.error && error.status != 500) {
              let err =  Object.keys(error.error)
              console.log(err)
              // currentAsignation.status = error.error.err
              if(error.error.detail){
                currentAsignation.status = error.error.detail
              } else if (error.error.transporter){
                currentAsignation.status = error.error.transporter[0]; 
              } else if (error.error.transporter_helper){
                currentAsignation.status = error.error.transporter_helper[0];
              }else{
                currentAsignation.status = "Error"
                console.log(error.error);
              }
            }else{
              currentAsignation.status = "Error"
              console.log(error);
            }
          }
        )

    } else {
      if (this.asigForm.value.barcode == "") {
        this.toastrService.warning("Este campo es requerido", "Codigo de barra");
      }
      if (this.asigForm.value.transporter == "") {
        this.toastrService.warning("Este campo es requerido", "Conductor");
      }
      if (this.asigForm.value.transport == "") {
        this.toastrService.warning("Este campo es requerido", "Pantente");
      }
      if(this.tipoVehiculo!="Moto"){
        if (this.asigForm.value.transporter_helper == "") {
          this.toastrService.warning("Este campo es requerido", "Peoneta");
        }
      }
    }

  }

  OpenAsig(){
    const DIAS = ["01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32"];

    let  fechahoy = new Date()
    let dia = DIAS[fechahoy.getDate()-1]
    let mes = DIAS[fechahoy.getMonth()]
    let año = fechahoy.getFullYear()
    let day = año+"-"+mes+"-"+dia

    if (this.asigForm.value.transporter == "") {
      this.toastrService.warning("Este campo es requerido", "Conductor");
    }else{
      this.modalService.dismissAll()
      this.router.navigate(['/operator/routes'], { queryParams: { type: 'auto', transporter: this.asigForm.value.transporter, log_date: day } ,  relativeTo: this.route});
    }
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  private doReload(){
    this.asignTable.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.clear().draw();
      dtInstance.rows.add(this.asigJson)
      dtInstance.columns.adjust().draw();
    })
  }

  search(search: string, item: any): boolean {
    return true;
  }


  selectPatente(event){
    // this.tipoVehiculo = this.lisPatente[parseInt(event)-1].type
    console.log(event)
    let res = this.lisPatente.find(res=> res.id == event)
    this.tipoVehiculo = res.type_display
    console.log(this.tipoVehiculo)
    console.log(res)

    if(this.tipoVehiculo=="Moto"){

      this.inputPeoneta = false  
      this.asigForm.value.transporter_helper == ""
    }else{  
      this.inputPeoneta = true
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
    this.dtTrigger.unsubscribe();
  }

}
