import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'; 
import { OperatorComponent } from "./operator.component";
import { DashboardOperatorComponent } from '../operator/dashboard-operator/dashboard-operator.component';
import { OperatorResolver } from "./operator.resolve";
import { TrackingComponent } from '../commons/tracking/tracking.component';
import { WoListComponent } from '../layouts/shared/wo-list/wo-list.component'
import { SolicitudOtComponent } from '../layouts/shared/solicitud-ot/solicitud-ot.component';
import { BulkComponent } from '../layouts/shared/solicitud-ot/bulk/bulk.component';
import { ImportdatafallaComponent } from '../operator/importdatafalla/importdatafalla.component';
import { AssignationReportComponent } from './assignation-report/generate/assignation-report.component';
import { RoutesComponent } from './routes/routes.component';
import { ARListComponent } from './assignation-report/list/list.component';
import { PrintFalabellaComponent } from './print-falabella/print-falabella.component';

const routes: Routes = [
  { 
    path: '',
    component: OperatorComponent,
    children: [
        {
          path: 'dashboard',
          component: DashboardOperatorComponent,
          data: {
            title: 'Listado de Solicitudes'
          } 
        },
        {
          path: 'falabella',
          component: ImportdatafallaComponent,
          data: {
            title: 'Carga Falabella'
          } 
        },
        {
          path: 'falabella/print',
          component: PrintFalabellaComponent,
          data: {
            title: 'Impresión Falabella'
          } 
        },
        {
          path: 'wolist',
          component: WoListComponent,
          data: {
            title: 'Workflow'
          } 
        },
        {
          path: 'wolist/tracking',
          component: TrackingComponent,
          data: { 
            group: 'b3BlcmF0b3Jz',
            title: 'Seguimiento'
          }
        },
        {
          path: 'solicitudot/new/bulk',
          component: BulkComponent,
          data: { 
            group: 'b3BlcmF0b3Jz',
            title: 'Carga Masiva'
          }
        },
        {
          path: 'solicitudot/:id',
          component: SolicitudOtComponent,
          resolve: { wo: OperatorResolver },
          data: { 
            group: 'b3BlcmF0b3Jz',
            title: 'Nueva Solicitud'
          }
        },
        {
          path: 'assignreport',
          component: AssignationReportComponent,
          data: {
            title: 'Generar Manifiesto'
          } 
        },
        {
          path: 'assignreport/list',
          component: ARListComponent,
          data: {
            title: 'Histórico Manifiestos'
          } 
        }
      ]
    },
    {
      path: 'routes',
      component: RoutesComponent
    }
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [ OperatorResolver,
  ]
})
export class OperatorRoutingModule { }
  