import { Component, OnInit ,NgZone,ElementRef,ViewChild} from '@angular/core';
import { FormBuilder,Validators} from '@angular/forms';
import { rutValidator } from '../../extra/validators';
import { MapsAPILoader } from '@agm/core';
import { Router, ActivatedRoute} from '@angular/router'
import { ToastrService } from 'ngx-toastr';
import { ServiceOperatorService } from "../service/service-operator.service";
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

const DetailError ={
  "sender_full_name"  : "Nombre completo origen",
  "receiver_full_name": "Nombre completo destinatario",
  "receiver_address"  : "Dirección destinatario", 
  "receiver_comune"   : "Comuna destinatario",
  "receiver_region"   : "Región destinatario",
  "sender_email"      : "Correo origen",
  "receiver_email"    : "Región destinatario",
  "height"            : "Alto",
  "width"             : "Ancho",
  "depth"             : "Largo",
  "weight"            : "Peso"
  }

@Component({
  selector: 'app-edit-ot',
  templateUrl: './edit-ot.component.html',
  styleUrls: ['./edit-ot.component.scss']
})
export class EditOtComponent implements OnInit {

  formOT = this.formBuilder.group({
    sender_full_name : ['',Validators.required],
    // sender_rut : ['',[rutValidator(), Validators.minLength(7)]],
    sender_rut : [''],
    sender_email: ['',[Validators.required,Validators.email]],
    sender_phone: ['',Validators.required],
    sender_address: ['',Validators.required],
    sender_comune: ['',Validators.required],
    sender_region: ['',Validators.required],
    sender_address_number: [''],
    sender_extra_info: [''],
    receiver_full_name: ['',Validators.required],
    receiver_rut: [''],
    receiver_email: ['',[Validators.required,Validators.email]],
    receiver_phone: ['',Validators.required],
    receiver_address: ['',Validators.required],
    receiver_comune: ['',Validators.required],
    receiver_region: ['',Validators.required],
    receiver_address_number: [''],
    deliver_info: [''],
    has_return: ['with_return',Validators.required],
    height: ['0',Validators.required],
    width: ['0',Validators.required],
    depth: ['0',Validators.required],
    weight: ['0',Validators.required],
    product: ['1',Validators.required],
    service: ['1',Validators.required],
    segment: ['1'],
    notification_type: ['none',Validators.required],
    // extra_fields: {}
})

@ViewChild("addressSearch")  public searchElementRef: ElementRef;
@ViewChild("addressSearch2")  public searchElementRef2: ElementRef;

public contacts = [];
id
public listService ;
public listaProducts ;
codBarra


private destroy$: Subject<boolean> = new Subject<boolean>();
  
  constructor(
    private formBuilder: FormBuilder,
    private mapsAPILoader : MapsAPILoader,
    private ngZone : NgZone,
    private activatedRoute  : ActivatedRoute,
    private toastrService  : ToastrService,
    private elementRef  : ElementRef,
    private OperatorService  : ServiceOperatorService,
    private router  : Router,
  ) { }

  ngOnInit() {
    this.id = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    console.log(this.id)
    
    this.getListService();
    this.listProducts();

    if (this.id)
      this.viewDataOt(this.id);

    // Direccion Origen
    this.mapsAPILoader.load().then( ()=> {
      let autocomplete = new google.maps.places.Autocomplete(
        this.searchElementRef.nativeElement, 
          { 
            types: ["address"],
            componentRestrictions: { country: "cl" }
          }
        );
      autocomplete.addListener("place_changed", ()=> {
        this.ngZone.run( ()=> {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
            if (place.geometry === undefined || place.geometry === null) {
              console.log("direccion mala");
              return;
            }
          let components = place.address_components;
          console.log(components);
          
          let addressValues
          console.log(components[0].types)
          if (components[0].types[0] === "street_number") {
             addressValues = {
              // sender_address_number: components.find(el => el.types.includes("street_number")).long_name,
              sender_address: components.find(el => el.types.includes("route")).long_name,
              sender_region: components.find(el => el.types.includes("administrative_area_level_2")).long_name,
              sender_comune: components.find(el => el.types.includes("administrative_area_level_3")).long_name
            }
          }else{
             addressValues = {
              // sender_address_number: components.find(el => el.types.includes("street_number")).long_name,
              sender_address: components.find(el => el.types.includes("route")).long_name,
              sender_region: components.find(el => el.types.includes("administrative_area_level_2")).long_name,
              sender_comune: components.find(el => el.types.includes("administrative_area_level_3")).long_name
            }
          }
          this.formOT.patchValue(addressValues);
          console.log(this.formOT);
          // console.log(this.formSV.valid);
        })
      });
    })
  
    // Direccion Destino 
    this.mapsAPILoader.load().then( ()=> {
      let autocomplete = new google.maps.places.Autocomplete(
        this.searchElementRef2.nativeElement, 
          { 
            types: ["address"],
            componentRestrictions: { country: "cl" }
          }
        );
      autocomplete.addListener("place_changed", ()=> {
        this.ngZone.run( ()=> {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
            if (place.geometry === undefined || place.geometry === null) {
              console.log("direccion mala");
              return;
            }
          let components = place.address_components;
          console.log(components);
          
          let addressValues
          console.log(components[0].types)
          if (components[0].types[0] === "street_number") {
             addressValues = {
              // receiver_address_number: components.find(el => el.types.includes("street_number")).long_name,
              receiver_address: components.find(el => el.types.includes("route")).long_name,
              receiver_region: components.find(el => el.types.includes("administrative_area_level_2")).long_name,
              receiver_comune: components.find(el => el.types.includes("administrative_area_level_3")).long_name
            }
          }else{
             addressValues = {
              // sender_address_number: components.find(el => el.types.includes("street_number")).long_name,
              receiver_address: components.find(el => el.types.includes("route")).long_name,
              receiver_region: components.find(el => el.types.includes("administrative_area_level_2")).long_name,
              receiver_comune: components.find(el => el.types.includes("administrative_area_level_3")).long_name
            }
          }
          this.formOT.patchValue(addressValues);
          console.log(this.formOT);
        })
      });
    })

  }

  getListService(){
    return this.OperatorService.listService()
    .subscribe((response :any)=>{
     this.listService = response.results
     console.log(this.listService)
    //  for(let item of this.listService)
    })
  }

  listProducts(){
    this.OperatorService.listProductsService()
    .subscribe((res : any)=>{
      this.listaProducts = res.results;
    })
  }

  viewDataOt(id){
    console.log(id);
    this.OperatorService.wiewListOtService(id).
    subscribe((response : any)=>{
      console.log(response)
      console.log(response.barcode)
      this.codBarra = response.barcode
      this.formOT.patchValue(response);
    }
    )
  }
  
  ReplaceOt(){
      this.OperatorService.replaceOt(this.id,this.formOT.value).
      subscribe(
        (response : any)=>{
          console.log(response);
          this.toastrService.success('Datos guardados correctamente','Solicitud actualizada') 
          // this.formOT.reset()  
          this.router.navigate(['../../../operator/dashboard'], { relativeTo: this.activatedRoute })
        },error=>{
          console.log(error.error);
          for(let errorHead in error.error){
            console.log(errorHead);
            this.toastrService.error(error.error[errorHead],DetailError[errorHead]);    
          }    
        }
        )
  }

  selectEvent(item) {
    // do something with selected item
    this.formOT.value.sender_full_name = item
    console.log(this.formOT)
  }
 
  onChangeSearch(val: string) {
    this.OperatorService.listContacService({ full_name__icontains: val})
    .pipe(
      takeUntil(this.destroy$)
    )
    .subscribe(
      (res : any) => {
        console.log(res)
        this.contacts = res.results;
      }
    )
  }
  
  onFocused(e){
    // do something when input is focused
    console.log("onFocused");
  }

}
