import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditOtComponent } from './edit-ot.component';

describe('EditOtComponent', () => {
  let component: EditOtComponent;
  let fixture: ComponentFixture<EditOtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditOtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditOtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
