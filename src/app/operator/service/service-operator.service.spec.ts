import { TestBed } from '@angular/core/testing';

import { ServiceOperatorService } from './service-operator.service';

describe('ServiceOperatorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServiceOperatorService = TestBed.get(ServiceOperatorService);
    expect(service).toBeTruthy();
  });
});
