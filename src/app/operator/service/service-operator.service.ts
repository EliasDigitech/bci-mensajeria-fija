import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http'
import {environment} from '../../../environments/environment'
import { tap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket'
import { XhrErrorHandlerService } from '../../services/xhr-error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class ServiceOperatorService {

  // private socketObject: BehaviorSubject<any>;
  // private socketObject: WebSocketSubject<any>;

  public connectSocket(path:string): WebSocketSubject<any>{
    // if (!this.socketObject) {
      return webSocket(environment.server_ipWebSocket + path +'/?token=' + localStorage.getItem('jtoken'))
    // }
    // return this.socketObject
  }

  constructor(
    private http: HttpClient,
    private xhrErrorHandlerService: XhrErrorHandlerService,
  ) { }
  private user:any;

  public replaceOt(id,dataRepalce){
    return this.http.put(
      environment.server_ip + 'wo/'+ id+'/', dataRepalce
    )
  }

  public addContacService(){
    return this.http.get(
      environment.server_ip + 'autocomplete/'
    )
  }

  public listOtService(consulta: any){
    return this.http.get(
      environment.server_ip +'wo/', { params: consulta }
    )
  }


  public listProductsService(){
    return this.http.get(
      environment.server_ip+'settings/products/'

    )
  }
  
  public wiewListOtService(id){
    return this.http.get(
      environment.server_ip + 'wo/'+ id+'/'
      )
  }

  public listService(){
    return this.http.get(
       environment.server_ip+'settings/services/'
     )
   }

  public listBikerService(id){
    return this.http.get(
      environment.server_ip +'companies/'+id+'/users/?groups=6'
    )
  }

  public IdConnexService(){
    if(this.user){
      return of(this.user)
    }
    return this.http.get(
      environment.server_ip +'user/'
    )
    .pipe(
      tap( res => {
        this.user = res;
      })
    )
  }

  public saveDashboardMdalServ(data){
    return this.http.put(
      environment.server_ip +'wo_process/', data 
    )
    
  }

  public listContacService(params){
    return this.http.get(
      environment.server_ip + 'autocomplete/',
      { params: params }
    )
  }

  csvFalabellaUpload(data: FormData){
    return this.http.post(environment.server_ip + "falabella/csv/", data)
      .pipe(
        catchError(this.xhrErrorHandlerService.handleError)
      )
  }

}
