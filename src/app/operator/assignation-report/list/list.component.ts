import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { LayoutService } from '../../../layouts/services/layout.service';
import { Subject, Observable, BehaviorSubject } from 'rxjs'
import { takeUntil, map, distinctUntilChanged, debounceTime, flatMap } from 'rxjs/operators';
import { DataTableDirective } from 'angular-datatables';
import { MetaTitleService } from '../../../services/meta-title.service';

@Component({
  selector: 'app-assignation-report-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ARListComponent implements OnInit, OnDestroy {
  @ViewChild(DataTableDirective, { static: true }) dtElement: DataTableDirective;

  transporters: Observable<any>;
  private currentCompany: number;
  private destroy$: Subject<boolean> = new Subject<boolean>();
  searchTerm: BehaviorSubject<string> = new BehaviorSubject<string>("");
  pageManifets: Array<any> = [];

  constructor(
    private formBuilder: FormBuilder,
    private layoutService: LayoutService,
    private router: Router,
    private route: ActivatedRoute,
    private metaTitleService: MetaTitleService,
  ) {
    this.metaTitleService.updateTitle();
  }

  assignForm = this.formBuilder.group({
    transporter: [''],
    created_at: [''],
  })

  dtOptions: any = {
    dom: 'bfrtip',
    searching: false,
    serverSide: true,
    processing: true,
    language: {
      url: '../../../assets/languages/Spanish.json'
    },
    columns: [
      {
        data: 'serie'
      },
      {
        data: 'created_at'
      },
      {
        data: 'transporter'
      },
      {
        data: 'user'
      },
    ],
    ajax: (data: any, callback) => {
      let consulta: any = {
        offset: data.start,
        limit: data.length,
        ...this.assignForm.value
      };
      this.layoutService.listAssignManifest(consulta)
        .pipe(
          takeUntil(this.destroy$)
        )
        .subscribe(
          (response: any) => {
            this.pageManifets = response.results;
            callback({
              recordsTotal: response.count,
              recordsFiltered: response.count,
              data: []
            });
          },
          error => {
            // console.log(error);
          }
        )
    },
  };

  search(search: string, item: any): boolean {
    return true;
  }

  ngOnInit(): void {
    this.layoutService.retieveUserInfo()
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(
        user => {
          this.currentCompany = user.branch.company_id;
        }
      )
    this.transporters = this.searchTerm
      .pipe(
        distinctUntilChanged(),
        debounceTime(400),
        flatMap(
          term => {
            return this.layoutService.getUsers(this.currentCompany, { groups: 6, ordering: 'first_name', search: term })
              .pipe(
                map(
                  (res: any) => {
                    return res.results;
                  }
                )
              )
          }
        )
      )

    this.assignForm.controls['transporter'].valueChanges
    .pipe(
      takeUntil(this.destroy$)
    )
      .subscribe(
        value => {
          if (value == null){
            this.assignForm.patchValue({'transporter': ''})
          }
        }
      )
  }

  retrieveAssign() {
    if (this.dtElement.dtInstance) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.ajax.reload();
      });
    }
  }

  openMann(url: string){
    window.open(url, '_blank');
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

}
