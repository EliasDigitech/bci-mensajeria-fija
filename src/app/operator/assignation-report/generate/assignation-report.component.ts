import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { LayoutService } from '../../../layouts/services/layout.service';
import { Subject, Observable, BehaviorSubject } from 'rxjs'
import { takeUntil, map, distinctUntilChanged, debounceTime, flatMap } from 'rxjs/operators';
import { DataTableDirective } from 'angular-datatables';
import { ToastrService } from 'ngx-toastr'
import { MetaTitleService } from '../../../services/meta-title.service';
@Component({
  selector: 'app-assignation-report',
  templateUrl: './assignation-report.component.html',
  styleUrls: ['./assignation-report.component.scss']
})
export class AssignationReportComponent implements OnInit, OnDestroy {
  @ViewChild(DataTableDirective, { static: true }) dtElement: DataTableDirective;

  transporters: Observable<any>;
  private currentCompany: number;
  private destroy$: Subject<boolean> = new Subject<boolean>();
  searchTerm: BehaviorSubject<string> = new BehaviorSubject<string>("");
  pageAssigns: Array<any> = [];
  today = new Date().toLocaleDateString();
  isLoading=false;

  constructor(
    private formBuilder: FormBuilder,
    private layoutService: LayoutService,
    private router: Router,
    private route: ActivatedRoute,
    private toastrService: ToastrService,
    private metaTitleService: MetaTitleService,
  ) { 
    this.metaTitleService.updateTitle();
  }

  assignForm = this.formBuilder.group({
    transporter: ['', Validators.required],
    log_date: ['', Validators.required],
    // all: ['']
  })

  dtOptions: any = {
    dom: 'bfrtip',
    searching: false,
    serverSide: true,
    processing: true,
    language: {
      url: '../../../assets/languages/Spanish.json'
    },
    columns: [
      {
        title: 'N°'
      },
      {
        title: 'Empresa'
      },
      {
        title: 'Origen'
      },
      {
        title: 'Destino'
      },
      {
        title: 'Fecha Creación'
      },
      {
        title: 'Días de atraso'
      },
      {
        title: 'Tipo servicio'
      },
    ],
    ajax: (data: any, callback) => {
      let consulta: any = {
        offset: data.start,
        limit: data.length,
        // excluded_status__in :'0',
        ...this.assignForm.value
      };
      // if(consulta.all){
      //   consulta.status__in = '2,5'
      // }
      if(!consulta.transporter){
        callback({
          recordsTotal: 0,
          recordsFiltered: 0,
          data: []
        });
      }else{
        this.layoutService.getLogs(consulta)
          .pipe(
            takeUntil(this.destroy$)
          )
          .subscribe(
            (response: any) => {
              this.pageAssigns = response.results;
              callback({
                recordsTotal: response.count,
                recordsFiltered: response.count,
                data: []
              });
            },
            error => {
              // console.log(error);
            }
          )
      }
    },
  };

  
  ngOnInit() {
    this.layoutService.retieveUserInfo()
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(
        user => {
          this.currentCompany = user.branch.company_id;
        }
      )
    this.transporters = this.searchTerm
        .pipe(
          distinctUntilChanged(),
          debounceTime(400),
          flatMap(
            term => {
              return this.layoutService.getUsers(this.currentCompany, { groups: 6, ordering: 'first_name', search: term })
                .pipe(
                  map(
                    (res: any) => {
                      return res.results;
                    }
                  )
                )
            }
          )
        )
    // this.assignForm.controls['all'].valueChanges
    // .pipe(
    //   takeUntil(this.destroy$)
    // )
    //   .subscribe(
    //     (value: boolean) => {
    //       if(value){
    //         this.assignForm.controls['log_date'].disable()
    //       }else{
    //         this.assignForm.controls['log_date'].enable()
    //       }
    //     }
    //   )
  }

  restaFechas = function(f1,f2)
  {
  const DIAS = ["01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32"];

  let  fechahoy = new Date()

  let dia = DIAS[fechahoy.getDate()]
  let mes = DIAS[fechahoy.getMonth()]
  let año = fechahoy.getFullYear()

  f1 = dia+"/"+mes+"/"+año

  var f1 = f1.replace("/","-")
  f1 = f1.replace("/","-")
  f2 = f2.replace("/","-")
  f2 = f2.substring(0,10);

  var aFecha1 = f1.split('-');
  var aFecha2 = f2.split('-');
  var fFecha1 = Date.UTC(aFecha1[2],aFecha1[1]-1,aFecha1[0]);
  var fFecha2 = Date.UTC(aFecha2[2],aFecha2[1]-1,aFecha2[0]);
  var dif = fFecha1 - fFecha2;
  var dias = Math.floor(dif / (1000 * 60 * 60 * 24));
  return dias-1;
  }

  search(search: string, item: any): boolean {
    return true;
  }

  retrieveAssign(){
    if (this.dtElement.dtInstance) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.ajax.reload();
      });
    }
  }
  
  printReport(){
    let a = confirm("¿Está seguro de emitir el manifiesto?");
    if (!a){
      return;
    }
    this.isLoading = true;
    this.layoutService.createAssignManifest(
      {
        route_polyline:"_",
        ...this.assignForm.value
      }

    )
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(
        res => {
          console.log(res)
          // this.alreadyGenerated = true;
          let objFra = document.createElement('iframe');
          objFra.style.visibility = 'hidden'
          objFra.src = window.URL.createObjectURL(res);
          document.body.appendChild(objFra);
          objFra.contentWindow.focus();
          objFra.contentWindow.print();
        },
        async err => {
          try {
            err.error = JSON.parse(await err.error.text())
          } catch { }
          this.isLoading = false;
          if (err.status === 500) {
            this.toastrService.error('Ha ocurrido un problema al generar el manifiesto');
          } else if (err.error instanceof Object) {
            for (let errorHead in err.error) {
              this.toastrService.error(err.error[errorHead]);
            }
          }
        }
      )
    // this.router.navigate(['../routes'], { relativeTo: this.route, queryParams: { type: 'auto', ...this.assignForm.value } })
  }

  ngOnDestroy(){
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

}
