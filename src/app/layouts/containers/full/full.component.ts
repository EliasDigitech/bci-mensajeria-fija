import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';

import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
// declare var $: any;
import { LayoutService } from '../../services/layout.service';
import { finalize, map, switchMap } from 'rxjs/operators';
import { timer } from 'rxjs';

import { RouteInfo } from '../../shared/sidebar/sidebar.metadata';

import { AuthService } from '../../../auth/services/auth.service';

@Component({
  selector: 'app-full-layout',
  templateUrl: './full.component.html',
  styleUrls: ['./full.component.scss']
})
export class FullComponent implements OnInit {

	public config: PerfectScrollbarConfigInterface = {};
  
  
  public innerWidth: any;
  public defaultSidebar: any;
  public showMobileMenu = false;
  public expandLogo = false;
  public sidebartype = 'full';
  public sideElems:any;
  public placeGhost = [];
  private EXTRA_FIELDS = ['%company_id', '%branch_id']

  constructor(
    public router: Router,
    private layoutService: LayoutService,
    private authService: AuthService,
    ) {}
  
  Logo() {
    this.expandLogo = !this.expandLogo;
  }

  ngOnInit() {
    let watchTimer = timer(500).subscribe( async () => {
      this.placeGhost = Array(7);
    });
    if (this.router.url === '/') {
      this.router.navigate(['/starter']);
    }
    this.defaultSidebar = this.sidebartype;
    this.handleSidebar();
    this.sideElems = 
      this.layoutService.retieveUserInfo()
      .pipe(
        switchMap(
          userInfo => {
            return this.layoutService.getSidebarElems()
            .pipe(
              map(
                (res: RouteInfo[]) =>{
                  this.placeGhost = [];
                  for(let el of res){
                    let result = this.testValue(el.path);
                    if (result.result){
                      el.path = el.path.replace(
                        result.chunk,
                        userInfo.branch[result.chunk.replace('%','')]
                      )
                    }
                  }
                  return res;
                }
              )
            )
          }
        ),
        finalize(
          () => {
            watchTimer.unsubscribe();
          }
        )
      );
  }
  private testValue(path:string):{ result:boolean, chunk?:string}{
    for(let extra of this.EXTRA_FIELDS){
      if (RegExp(extra).test(path)){
        return { result: true, chunk: extra };
      }
    }
    return {result: false}
  }
  
  logout(){
    this.authService.logout();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.handleSidebar();
  }

  handleSidebar() {
    this.innerWidth = window.innerWidth;
    if (this.innerWidth < 1170) {
      this.sidebartype = 'mini-sidebar';
    } else {
      this.sidebartype = this.defaultSidebar;
    }
  }

  toggleSidebarType() {
    switch (this.sidebartype) {
      case 'full':
        this.sidebartype = 'mini-sidebar';
        break;

      case 'mini-sidebar':
        this.sidebartype = 'full';
        break;

      default:
    }
  }
}
