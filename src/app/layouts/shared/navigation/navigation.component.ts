import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthService } from '../../../auth/services/auth.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  @Output()
  toggleSidebar = new EventEmitter<void>();

  public showSearch = false;

  nameNavigation 

  constructor(
    private authService: AuthService,
  ) {}

  ngOnInit() {
    this.viewUser()
  }

  logout() {
    this.authService.logout();
  }

  viewUser(){
    this.authService.retrieveUserInfo().subscribe((res:any)=>{
      // console.log(res);
      this.nameNavigation = res.first_name;
    },error=>{
      console.log(error);
    })
  }
}
