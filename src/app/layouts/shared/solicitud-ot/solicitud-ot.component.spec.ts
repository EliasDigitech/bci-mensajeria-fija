import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitudOtComponent } from './solicitud-ot.component';

describe('SolicitudOtComponent', () => {
  let component: SolicitudOtComponent;
  let fixture: ComponentFixture<SolicitudOtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitudOtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitudOtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
