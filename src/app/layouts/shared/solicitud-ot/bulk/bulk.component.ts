import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { WoService } from '../../../services/wo-service.service';
import { ToastrService } from 'ngx-toastr';
import { Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { LayoutService } from '../../../services/layout.service'
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-bulk',
  templateUrl: './bulk.component.html',
  styleUrls: ['./bulk.component.scss']
})
export class BulkComponent implements OnInit, OnDestroy {
  @ViewChild('woFile') woFile: ElementRef;

  public isLoading: boolean = false;
  public uploadResult: any = { type: 'waiting', human: 'En Espera'};
  public isElevated = false;
  public companies:Observable<any>;
  private destroy$: Subject<boolean> = new Subject<boolean>();
  selectedCompany:number;

  FIELDS = {
    'sender_full_name': 'Remitente',
    'sender_rut': 'RUT Remitente',
    'sender_email': 'Email Remitente',
    'sender_phone': 'Teléfono Remitente',
    'sender_address': 'Dirección Remitente',
    'sender_address_number': 'Dpto/Oficina Remitente',
    'sender_extra_info': 'Comentarios',
    'product_description': 'Descripción de producto',
    'receiver_full_name': 'Destinatario',
    'receiver_rut': 'RUT Destinatario',
    'receiver_email': 'Email Destinatario',
    'receiver_phone': 'Teléfono Destinatario',
    'receiver_address': 'Dirección Destinatario',
    'receiver_address_number': 'Dpto/Oficina Destinatario',
    'has_return': 'Retorno',
    'height': 'Alto',
    'width': 'Ancho',
    'depth': 'Largo',
    'weight': 'Peso',
    'product': 'Producto',
    'service': 'Servicio',
    'segment': 'Tramo',
    'extra_fields': 'Campos Extras',
    'sender_lat' : 'Latitud Remitente',
    'sender_lng' : 'Longitud Remitente',
    'receiver_lat' : 'Latitud Destinatario',
    'receiver_lng' : 'Longitud Destinatario'
  }

  constructor(
    private woService: WoService,
    private toastrService: ToastrService,
    private layoutService: LayoutService,
    private activatedRoute : ActivatedRoute,
    ) {
      this.activatedRoute.data
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(
        (data:any) => {
          if (data.group === 'b3BlcmF0b3Jz'){
            this.isElevated = true;
          }
        })
    }

  ngOnInit() {
    this.companies = this.layoutService.getCompanies({limit: 999})
    .pipe(
      map(
        (res: any) => {
          return res.results;
        }
      ),
      takeUntil(this.destroy$)
      )
  }

  beginUpload(){
    let fd = new FormData()
    if (this.isElevated){
      if(!this.selectedCompany){
        this.toastrService.warning("Debe seleccionar empresa");
        return
      }
      fd.append("company", this.selectedCompany.toString());
    }
    let native = this.woFile.nativeElement
    fd.append("wo_file", native.files[0])
    this.uploadResult = { type: 'loading', human:'Cargando' };
    this.isLoading = true;
    this.woService.bulkUpload(fd)
    .pipe(
      takeUntil(this.destroy$)
    )
      .subscribe(
        (res: any) => {
          this.isLoading = false;
          this.uploadResult = {
            type: 'success',
            human: 'Exitoso',
            details: {
              qty: res.length,
              barcodes: res.map(el => el.barcode).toString()
            }
          }
        },
        (err: any) => {
          this.isLoading = false;
          if (err.status == 400){
            let errors = [];
            if (Array.isArray(err.errMsg)){
              for (let i=0; i<err.errMsg.length; i++){
                let currentMsg = [];
                if (Object.keys(err.errMsg[i]).length == 0){
                  continue;
                }
                for(let key in err.errMsg[i]){
                  currentMsg.push({
                    field: this.FIELDS[key],
                    detail: err.errMsg[i][key]
                  })
                }
                errors.push({
                  row: i + 2,
                  messages: currentMsg
                })
                this.uploadResult = {
                  type : 'error',
                  human: 'Error',
                  details: errors
                }
              }
            }else{
              this.uploadResult = {
                type: 'error',
                human: err.errMsg.wo_file[0],
              }
            }
          }else{
            this.uploadResult = {
              type: 'error',
              human: 'Ha ocurrido un problema al cargar el archivo'
            }
          }
        }
      )
  }

  downloadTemplate() {
    this.woService.downloadTemplate()
    .pipe(
      takeUntil(this.destroy$)
    )
      .subscribe(
        res => {
          let anchor = document.createElement('a');
          anchor.download = "WO_template.xlsx";
          anchor.href = window.URL.createObjectURL(res);
          anchor.dataset.downloadurl = ['text/plain', anchor.download, anchor.href].join(':');
          anchor.click();
        },
        () => {
          this.toastrService.error("No es posible generar el modelo de carga")
        }
      )
  }

  ngOnDestroy(){
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
}
