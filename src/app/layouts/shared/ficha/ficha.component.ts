import { Component, OnInit ,ElementRef,ViewChild, NgZone, Renderer2 } from '@angular/core';
import { FormBuilder, Validators, FormArray } from '@angular/forms';
import { Segments, Services, Pricings} from '../../../admin/models';
import { LayoutService} from '../../services/layout.service'
import { forkJoin } from 'rxjs';
import { pairwise, startWith } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { rutValidator } from '../../../extra/validators';
import { MapsAPILoader } from '@agm/core';
import { ActivatedRoute, Router } from "@angular/router";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { MetaTitleService } from '../../../services/meta-title.service';

  const FIELDS =  {
    "email": "Correo",
    "username": "Nombre de usuario",
    "company_name": "Nombre compañia",
    "commercial_business": "Giro comercial",
    "address": "DIrección",
    "phone": "Telefono",
    "contact_first_name": "Nombre contacto",
    "contact_last_name": "Apellido contacto",
    "contact_email": "Email",
    "comune":  "Comua",
    "province":  "provincia",
    "region":  "región",
    "status":  "Estado",
    "billing_contacts" : "contacto de pagos",
    "file_pricings" : "Lista de precio",
    "extra_fields" : "Imformación extra"
  }

  const FINAL_STATUSES = [
    'accepted',
    'rejected'
  ];
  

  @Component({
    selector: 'app-ficha',
    templateUrl: './ficha.component.html',
    styleUrls: ['./ficha.component.scss']
  })
  
export class FichaComponent implements OnInit {

  @ViewChild("addressSearch", {static: true})  public searchElementRef: ElementRef;
  @ViewChild("contentModal", {static: false})  public contentModal: ElementRef;
  
  segments : Segments [] = new Array <Segments> ();
  services : Services [] = new Array <Services> ();
  pricings : Pricings [] = new Array <Pricings> ();

  id: string = null;
  disableStatus = false;
  formDisabled = false;
  isCompleted = false;
  hasApprovement = false;
  hasSave = false;
  isLoading = false;
  buttonSaveSent = false;
  idUser; 
  closeResult = '';
  Dataform;
  precioGuardados;
  // buttonSaveSent = true;
  STATUS = {
    accepted: 'Aprobada',
    rejected: 'Rechazada'
  }

  companyForm = this.fb.group({
    company_name: ['', Validators.required],                 
    commercial_business: ['', Validators.required],
    rut: ['', [Validators.required, rutValidator(), Validators.minLength(7)]],
    address: ['', Validators.required],
    address_number: [''],
    phone: ['', Validators.required],
    contact_first_name: ['', Validators.required],
    contact_last_name: ['', Validators.required],
    contact_email: ['', [Validators.required,Validators.email]],
    comune: ['', Validators.required],
    province: ['', Validators.required],
    region: ['', Validators.required],
    lat: ['', Validators.required],
    lng: ['', Validators.required],
    status: [''],
    billing_contacts : this.fb.array([]),
    file_pricings : this.fb.array([]),
    extra_fields : this.fb.array([]),
  })
  
  constructor(
    private fb: FormBuilder,
    private layoutService : LayoutService,
    private toastrService : ToastrService,
    private mapsAPILoader : MapsAPILoader,
    private ngZone : NgZone,
    private activatedRoute : ActivatedRoute,
    private router : Router,
    private modalService: NgbModal,
    private renderer: Renderer2,
    private metaTitleService: MetaTitleService,

  ) {
    this.metaTitleService.updateTitle();

    this.idUser = window.location.pathname.split("/")
  
    this.addBillingInfo();
    
  }

  ngOnInit() {

    // this.ViewPricings();
    
    forkJoin(
      this.layoutService.segments({ default: 'yes' }),
      this.layoutService.services({ default: 'yes' })
    )
    .subscribe(
      (responses: any) => {
        this.segments = responses[0].results;
        this.services = responses[1].results;
        for(let segment of this.segments){
          for(let service of this.services){
            this.addFilePricingInfo(segment.id, service.id);
          }
        }
        this.mostrarPricing();  
      }
    )

    this.activatedRoute.data
      .subscribe(
        data => {
          console.log(data.file)
          this.hasApprovement = data.group == 'YWRtaW5z';
          if(data.file == 'new'){
            this.hasSave = true;
          }else{
            this.companyForm.patchValue(data.file);
            this.precioGuardados = data.file.file_pricings;
            this.file_pricings.patchValue(data.file.file_pricings);
            console.log(this.companyForm.value)
            console.log(this.file_pricings)
            console.log(data.file.file_pricings)
   
            this.disableStatus = true;
            this.id = data.file.id;
            if (this.idUser[1] !== "adm"){
              console.log(this.idUser)
              this.formDisabled = FINAL_STATUSES.includes(data.file.status) || data.file.status == 'sent';
            }
            this.isCompleted = FINAL_STATUSES.includes(data.file.status);
            this.hasSave = !FINAL_STATUSES.includes(data.file.status) && data.group == 'dHJhZGVycw==';
            for (let i = 0; i < data.file.extra_fields.length; i++) {
              this.addExtraField();
            }
            for (let i = 0; i < data.file.billing_contacts.length -1; i++) {
              this.addBillingInfo()
            }
            this.companyForm.patchValue(data.file); 
            console.log(this.companyForm.value)
            this.Dataform = this.companyForm.value
          }
        }
      )


    if(parseInt(this.activatedRoute.snapshot.params['id'])){
      this.buttonSaveSent = true
    }


    this.companyForm.controls['address'].valueChanges
      .pipe(
        startWith(""),
        pairwise()
      )
      .subscribe(
        ([prev, next]) => {
          if(prev != next){
            this.companyForm.controls['address'].setErrors({required: true})
          }
        }
      )

    this.mapsAPILoader.load().then( ()=> {
      let autocomplete = new google.maps.places.Autocomplete(
        this.searchElementRef.nativeElement, 
        { 
          types: ["address"],
          componentRestrictions: { country: "cl" }
        }
        );
        
      autocomplete.addListener("place_changed", ()=> {
        this.ngZone.run( ()=> {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          if (place.geometry === undefined || place.geometry === null) {
            this.companyForm.controls['address'].setErrors({required: true})
            return;
          }
          let components = place.address_components;
          let addressValues = {
            address: place.name,
            comune: components.find(el => el.types.includes("administrative_area_level_3")).long_name,
            province: components.find(el => el.types.includes("administrative_area_level_2")).long_name,
            region: components.find(el => el.types.includes("administrative_area_level_1")).long_name,
            lat: Math.round(place.geometry.location.lat() * 10000000) / 10000000,
            lng: Math.round(place.geometry.location.lng() * 10000000) / 10000000,
          }
          this.companyForm.patchValue(addressValues);
          this.companyForm.controls['address'].setErrors({required: null});
          this.companyForm.controls['address'].updateValueAndValidity({onlySelf:true});
        })
      });
    })
  }
  
  open(content?) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      this.router.navigate(['../'], { relativeTo: this.activatedRoute });
    }, (reason) => {
      // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  deleteFile(){
    this.layoutService.deleteFileService(this.id)
    .subscribe((response : any)=>{
      this.toastrService.success('', 'Ficha eliminada correctamente');
      this.router.navigate(['../'], { relativeTo: this.activatedRoute });
    },
    error=>{
      for(let element in error.error)
        this.toastrService.error(error.error[element],element);
      }
    ) 
  }

  sendToApprove(){
    this.isLoading = true;
    this.companyForm.value.status = 'sent';
    let idFicha = this.activatedRoute.snapshot.params['id']
    this.layoutService.sendStatusService(idFicha,this.companyForm.value)
      .subscribe((response : any)=>{
        this.router.navigate(['../'], { relativeTo: this.activatedRoute });
        this.isLoading = false;
      },
      error => {
        this.isLoading = false;
        console.log(error)
        for(let err in error.error){
          this.toastrService.error(error.error[err],'Error');
        }
          
      }
     )
  }

  sendStatus(status: string) {
      this.isLoading = true;
      this.layoutService.updateFile(this.id, { status: status })
        .subscribe(
          () => {
            this.toastrService.success('Ficha ' + this.STATUS[status] + ' correctamente');
            this.router.navigate(['../'], { relativeTo: this.activatedRoute })
            this.isLoading = false;
          },
          error => {
            for (let err in error.errMsg) {
              this.toastrService.error(error.errMsg[err]);
            }
            this.isLoading = false;
          }
        )
    }

  createFicha(){


    this.companyForm.value.company_name = this.companyForm.value.company_name[0].toUpperCase() + this.companyForm.value.company_name.slice(1);                
    this.companyForm.value.commercial_business =  this.companyForm.value.commercial_business[0].toUpperCase() + this.companyForm.value.commercial_business.slice(1);                
    this.companyForm.value.contact_first_name =  this.companyForm.value.contact_first_name[0].toUpperCase() + this.companyForm.value.contact_first_name.slice(1);                
    this.companyForm.value.contact_last_name =  this.companyForm.value.contact_last_name[0].toUpperCase() + this.companyForm.value.contact_last_name.slice(1);                 
    
    for(let b in this.companyForm.value.billing_contacts){
      this.companyForm.value.billing_contacts[b].name = this.companyForm.value.billing_contacts[b].name[0].toUpperCase() + this.companyForm.value.billing_contacts[b].name.slice(1);
       }

    this.isLoading = true;
    if (this.id === null ){
      this.companyForm.value.status ="draft";
      console.log(this.companyForm.value)
      this.layoutService.create_company_files(this.companyForm.value)
      .subscribe(
        (response :any)=>{
          this.isLoading = false;
          this.toastrService.success('', 'Datos ingresados correctamente', { timeOut: 6000 });
          // this.router.navigate(['../'], { relativeTo: this.activatedRoute });
          this.Dataform =response;
          this.open(this.contentModal)
        },
          error=>{
            // console.log(error);
            this.isLoading = false;
            if (error.error && (error.status !=500 && error.status !=0)  ){
              for(var errorHead in error.error){
                this.toastrService.error(error.error[errorHead],FIELDS[errorHead])
                // this.toastrService.error(error.error[errorHead],FIELDS[errorHead])
              }
            }else if(error.status ==0){
              this.toastrService.error('Error de conexión','')
            }
          }
        )
    }else{
      console.log(this.id)
      console.log(this.companyForm.value)
      this.layoutService.update_company_files(this.id,this.companyForm.value)
      .subscribe((response :any)=>{
        this.isLoading = false;
        this.toastrService.success('', 'Datos actualizados correctamente', { timeOut: 6000 });
        // this.router.navigate(['../'], { relativeTo: this.activatedRoute });
        this.Dataform =response;
        this.open(this.contentModal)
      },
      error=>{
        this.isLoading = false;
        if (error.error && (error.status !=500 && error.status !=0 && error.status !=400) ){
          for(var errorHead in error.error){
            this.toastrService.error(error.error[errorHead],FIELDS[errorHead])
          }
        }else if(error.status ===0){
          this.toastrService.error('Error de conexión',' ')
        }else if(error.status ===400){
          for(let err in error.error)
          this.toastrService.error(error.error[err],'Error')
        }
        }
      )
    }
  }

  generate() {
    let a = confirm("Al generar la ficha se creará automáticamente el cliente. Asegúrese que todo los datos estén correctamente ingresados");
    if(a){
      this.createFicha();
    }
  }

  mostrarPricing(){
    this.layoutService.pricings({ default: 'yes' }).subscribe((response : any)=>{
      this.pricings = response
      if(this.precioGuardados){
        this.file_pricings.patchValue(this.precioGuardados);
      }else{
        this.file_pricings.patchValue(this.pricings);
      }
    })
  }
   
  Viewsegments() {
    this.layoutService.segments().
    subscribe((response : any) => {
      this.segments = response.results;
      },
      error=>{
        for(let element in error.error)
                this.toastrService.error(error.error[element],element);
        }
      )
  }  
  
  ViewPricings() {
    this.layoutService.pricings({ default: 'yes' }).subscribe((response : any) => {
      this.pricings = response.results;
        },
      error=>{
        for(let element in error.error){
          // this.toastrService.error(error.error[element],element);
        }
      }
    )
  }
    
  ViewServices(){
    this.layoutService.services().subscribe((response : any) =>{
      this.services = response.results;
    },
    error=>{
      for(let element in error.error){
        this.toastrService.error(error.error[element],element);
      }
    }
    )
  }

  get billing_contacts(){
    return this.companyForm.get('billing_contacts') as FormArray
  }
  
  get file_pricings(){
    return this.companyForm.get('file_pricings') as FormArray
  } 

  get extra_fields(){
    return this.companyForm.get('extra_fields') as FormArray
  }

  getCurrentIndex(y:number, x:number){
    return (y + x) + ((this.services.length - 1)*y);
  }

  addBillingInfo(){
    if(this.billing_contacts.controls.length < 3){
      this.billing_contacts.push(
        this.fb.group({
          name: [''], 
          email: ['', [Validators.required]],
          phone: [''],
          address: [''],
        }));
    }
  }

  addFilePricingInfo(segment:number, service:number){
    this.file_pricings.push(
      this.fb.group({
        no_return: [''],
        with_return: [''],
        min_quantity: [''],
        segment: [segment],
        service: [service],
      })
    )
  }

  addExtraField(){
    if (this.extra_fields.controls.length < 4) {
      this.extra_fields.push(
        this.fb.group({
          type: ['text', Validators.required],
          name: ['', Validators.required],
        })
      )
    }
  }
  
  removeExtraField(index: number){
    this.extra_fields.removeAt(index);
  }
}

