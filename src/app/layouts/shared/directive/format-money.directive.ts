import { Directive ,ElementRef,Input,HostListener} from '@angular/core';
import { CurrencyPipe } from '@angular/common';

@Directive({
  selector: 'input[FormatMoney]'
})
export class FormatMoneyDirective {

  constructor(
    public elementRef : ElementRef,
    private currencyPipe : CurrencyPipe,
    ) {}
    
    @Input() valueDirective: string;

  // @HostListener("focus", ["$event.target"])
  // onFocus(target) {
  //   console.log('onFocus : '+target.value)
  // }

  // @HostListener("input", ["$event.target.value"])
  // onInput(value) {
  //   console.log('onInput : '+value)
  //   // this.cambiarcolor(value);
  // }

  initalValue
@HostListener('document:click', ['$event'])
runThisMethod() {
  if(this.elementRef.nativeElement.value !=''){
    let numInit = this.elementRef.nativeElement.value;
    numInit = numInit.replace('$','');
    numInit = numInit.replace(',','');
    numInit = numInit.replace(' ','');
    numInit = numInit.replace(/[^0-9]*/g, '');
  let dataLength = this.currencyPipe.transform(numInit,'$ ').length;
  this.elementRef.nativeElement.value = this.currencyPipe.transform(numInit,'$ ').substr(0,dataLength-3)
  this.initalValue = numInit;
   }
}

// OnInit() {
//     if(this.elementRef.nativeElement.value !=''){
//     let numInit = this.elementRef.nativeElement.value;
//     numInit = numInit.replace('$','');
//     numInit = numInit.replace(',','');
//     numInit = numInit.replace(' ','');
//     numInit = numInit.replace(/[^0-9]*/g, '');
//   let dataLength = this.currencyPipe.transform(numInit,'$ ').length;
//   this.elementRef.nativeElement.value = this.currencyPipe.transform(numInit,'$ ').substr(0,dataLength-3)
//   this.initalValue = numInit;
//    }
//     }
   


  @HostListener('input', ['$event']) onInputChange(event) {
     this.initalValue = this.elementRef.nativeElement.value;
     this.initalValue = this.initalValue.replace('$','');
     this.initalValue = this.initalValue.replace(',','');
     this.initalValue = this.initalValue.replace(' ','');
     this.initalValue = this.initalValue.replace(/[^0-9]*/g, '');
     if ( this.initalValue !== this.elementRef.nativeElement.value) {
       event.stopPropagation();
    }
  }

  @HostListener("blur", ["$event.target"]) onBlur(target) {
    console.log('onBlur :'+target.value)
    if(this.initalValue=='')
    this.initalValue = this.elementRef.nativeElement.value;
    this.initalValue = this.initalValue.replace('$','');
    this.initalValue = this.initalValue.replace(',','');
    this.initalValue = this.initalValue.replace(' ','');
    this.initalValue = this.initalValue.replace(/[^0-9]*/g, '');
      
    let dataLength = this.currencyPipe.transform(this.initalValue,'$ ').length;
    this.elementRef.nativeElement.value = this.currencyPipe.transform(this.initalValue,'$ ').substr(0,dataLength-3)
    console.log('toLocaleString :'+this.currencyPipe.transform(this.initalValue) )
  }

  
  }
