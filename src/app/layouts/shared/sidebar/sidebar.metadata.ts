// Sidebar route metadata
export interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  css_class: string;
  extralink: boolean;
  parent_available: boolean;
  children: RouteInfo[];
}
