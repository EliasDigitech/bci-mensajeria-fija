import { TestBed } from '@angular/core/testing';

import { WoService } from './wo-service.service';

describe('WoServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WoService = TestBed.get(WoService);
    expect(service).toBeTruthy();
  });
});
