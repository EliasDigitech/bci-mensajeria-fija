export class Segments{
    id: number;
    name: string;
    height: string;
    width: string;
    depth: string;
    min_weight: string;
    max_weight: string;
}

export class Services{
    id: number;
    name: string;
    status: string;
}

export class Pricings{
    id: number;
    is_default:string;
    no_return: string;
    with_return: string;
    min_quantity: string;
    segment: string;
    service: string;
    company: string;
}

export class BillingInfo {
    name: string;
    email:string;
    phone?:string;
    address?:string;
}

export class CreateFile{
    company_name: string;
    commercial_business : string;
    rut: string;
    address: string;
    phone: string;
    contact_first_name: string;
    contact_last_name: string;
    contact_email: string;
    comune: string;
    province: string;
    region: string;
    billing_contacts: BillingInfo [] = new Array <BillingInfo>() ;
    file_pricings: any;
}