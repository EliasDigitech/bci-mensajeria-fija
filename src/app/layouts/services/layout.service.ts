import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { RouteInfo } from '../shared/sidebar/sidebar.metadata';
import { environment } from '../../../environments/environment';
import { map, catchError, tap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { CreateFile } from './models' 

import { XhrErrorHandlerService } from '../../services/xhr-error-handler.service';

@Injectable()
export class LayoutService {

  private sidebarElems: RouteInfo[];
  private userInfo: any;
  constructor(
    private http: HttpClient,
    private xhrErrorHandlerService: XhrErrorHandlerService,
  ) { }

  getSidebarElems():Observable<RouteInfo[]>{
    if(this.sidebarElems){
      return of(this.sidebarElems);
    }
    return this.http.get(environment.server_ip + 'render/')
    .pipe(
      map(
        (results:RouteInfo[]) =>  {
          return results;
        }
      )
    )
  }

  canAccessRoute(segment:string):Observable<boolean>{
    return this.http.post(environment.server_ip + 'render/', { segment: segment})
    .pipe(
      map( _=> true ),
      catchError( _=> of(false))
    )
  }

  public getPatente(params){
    return this.http.get(
      environment.server_ip + 'settings/transports/', { params: params}
      )
  } 

  public asigPatente(data){
    return this.http.post(
      environment.server_ip + 'settings/transports/',data
      )
  } 
  
  public segments( params?: any): Observable<any>{
    return this.http.get(
      environment.server_ip + 'settings/segments/',
      { params: params}
      )
  }

  public services( params?: any){
    return this.http.get(
      environment.server_ip + 'settings/services/',
      { params: params}
      )
  }

  public sendStatusService(id,form : CreateFile){
    // console.log(id);
    // console.log(form);
    if(parseInt(id)){
      return this.http.put(
        environment.server_ip + 'company_files/'+id+'/',form
        )
      }else{
        return this.http.post(
          environment.server_ip + 'company_files/',form
          )
      }
  }

  public update_company_files( id,a :CreateFile){
    return this.http.patch(
      environment.server_ip +'company_files/'+id+'/',a
    )
  }

  public create_company_files( a :CreateFile){
    // console.log(a);
    return this.http.post(
      environment.server_ip +'company_files/',a
    )
  }

  public pricings(params?:any){
    return this.http.get(
      environment.server_ip + 'settings/pricings/',
      { params: params }
      )
  }

  public viewEditFile(id){
    return this.http.get( 
      environment.server_ip + 'company_files/'+id+'/')
  }

  public retieveUserInfo(){
    if (this.userInfo) {
      return of(this.userInfo)
    }
    return this.http.get(
      environment.server_ip + 'user/'
    ).pipe(
      tap(
        res => {
          this.userInfo = res;
        }
      )
    )
  }
  public deleteFileService(id){
    // console.log(id)
    return this.http.delete(
      environment.server_ip + 'company_files/'+id+'/' )
  }

  public addContacService(){
    return this.http.get(
      environment.server_ip + 'autocomplete/'
    )
  }


  // Files related
  public updateFile(id: string, data: any) {
    return this.http.patch(environment.server_ip + 'company_files/' + id + '/', data)
      .pipe(
        catchError(this.xhrErrorHandlerService.handleError)
      );
  }

  public getCompanies(params?: any) {
    return this.http.get(
      environment.server_ip + 'companies/',
      { params: params }
    )
  }

  public getBranches(companyId: number, params?:any){
    return this.http.get(
      environment.server_ip + 'companies/' + companyId + '/branches/', { params: params }
    )
  }

  public getUsers(companyId: number, params?: any){
    return this.http.get(
      environment.server_ip + 'companies/' + companyId + '/users/', { params: params }
    )
  }

  public listWO(consulta: any) {
    return this.http.get(
      environment.server_ip + 'wo/', { params: consulta }
    )
  }

  public wiewWo(id: Number){
    return this.http.get(
      environment.server_ip + 'wo/' + id + '/'
    ).pipe(
      catchError(this.xhrErrorHandlerService.handleError)
    )
  }

  // Assign manifests

  public getLogs(params: any) {
    return this.http.get(environment.server_ip + 'wo/reports/list/', { params: params })
      .pipe(
        catchError(this.xhrErrorHandlerService.handleError)
      )
  }

  public listAssignManifest(params: any){
    return this.http.get(environment.server_ip + 'wo/assign/', { params: params })
      .pipe(
        catchError(this.xhrErrorHandlerService.handleError)
      )
  }

  public createAssignManifest(data: any){
    return this.http.post(environment.server_ip + 'wo/assign/', data, { responseType: 'blob' })
      // .pipe(
      //   catchError(
      //     async err => {
            
      //       return err
      //     })
      // ) 
  }

}
