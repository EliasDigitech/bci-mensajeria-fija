import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { XhrErrorHandlerService } from '../../services/xhr-error-handler.service'
import { catchError } from 'rxjs/operators';
import { Observable} from 'rxjs'

@Injectable()
export class WoService {

  constructor(
    private http: HttpClient,
    private xhrErrorHandlerService: XhrErrorHandlerService
  ) { }

  bulkUpload(data: FormData){
    return this.http.post(environment.server_ip + "wo/upload/", data)
      .pipe(
        catchError(this.xhrErrorHandlerService.handleError)
      )
  }

  downloadTemplate(): Observable<Blob> {
    return this.http.get(environment.server_ip + 'wo/upload_template/', { responseType: 'blob' })
  }

  public saveOtService(formOt: any) {
    return this.http.post(
      environment.server_ip + 'wo/', formOt
    ).pipe(
      catchError(this.xhrErrorHandlerService.handleError)
    )
  }

  public replaceOt(id: number, dataRepalce: any) {
    return this.http.put(
      environment.server_ip + 'wo/' + id + '/', dataRepalce
    ).pipe(
      catchError(this.xhrErrorHandlerService.handleError)
    )
  }

  public patchOT(id:number, data: any){
    return this.http.patch(
      environment.server_ip + 'wo/' + id + '/', data
    ).pipe(
      catchError(this.xhrErrorHandlerService.handleError)
    )
  }

  public listService() {
    return this.http.get(
      environment.server_ip + 'settings/services/'
    ).pipe(
      catchError(this.xhrErrorHandlerService.handleError)
    )
  }

  public listProductsService() {
    return this.http.get(
      environment.server_ip + 'settings/products/'
    ).pipe(
      catchError(this.xhrErrorHandlerService.handleError)
    )
  }

  public listSegments() {
    return this.http.get(
      environment.server_ip + 'settings/segments/', { params: { limit: "999" } }
    ).pipe(
      catchError(this.xhrErrorHandlerService.handleError)
    )
  }

  public extra_fields(params?:any) {
    return this.http.get(
      environment.server_ip + 'settings/extra_fields/',
      { params: params }
    ).pipe(
      catchError(this.xhrErrorHandlerService.handleError)
    )
  }

  public listContacService(params?: any) {
    return this.http.get(
      environment.server_ip + 'autocomplete/',
      { params: params }
    ).pipe(
      catchError(this.xhrErrorHandlerService.handleError)
    )
  }
}
