import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { ServicesService } from "../services.service";
import { Profile, PassReset } from "../../layouts/models";
import { FormBuilder,Validators } from "@angular/forms";
import { ToastrService } from 'ngx-toastr';
import { PasswordValidation } from '../../extra/validators';
import { Subject } from 'rxjs';
import { takeUntil, map } from 'rxjs/operators';
import { trigger, transition, style, animate } from '@angular/animations'
import { MetaTitleService } from '../../services/meta-title.service';

const nameError = {
  old_password : "Constraseña Actual",
  password : "Nueva contraseña",
  confirm_password : "Confirmar contraseña",
}


@Component({
  selector: 'app-view-profile',
  templateUrl: './view-profile.component.html',
  styleUrls: ['./view-profile.component.scss'],
  animations: [
    trigger(
      'inOutAnimation',
      [
        transition(
          ':enter',
          [
            style({ opacity: 0 }),
            animate('0.5s ease-out',
              style({ opacity: 0.3 }))
          ]
        ),
        transition(
          ':leave',
          [
            style({ opacity: 0.3 }),
            animate('0.5s ease-in',
              style({ opacity: 0 }))
          ]
        )
      ]
    )
  ]
})
export class ViewProfileComponent implements OnInit, OnDestroy {
  @ViewChild('pictureFile') pictureFile: ElementRef;

  formProfile = this.formb.group({
    id:['',Validators.required],
    username: [{ value: '', disabled: true}],
    first_name:['',Validators.required],
    last_name:['',Validators.required],
    rut:[{value:'', disabled: true}],
    phone:['',Validators.required],
    email: [{ value: '', disabled: true}],
    // branch : this.formb.array([])
  })

  formPass = this.formb.group({
    old_password : ['',Validators.required],
    password : ['',[Validators.minLength(8),Validators.maxLength(50)]],
    confirm_password: ['',Validators.required],
  }, { validator: PasswordValidation.MatchPassword }
  )
  
  skeletonloader = true ;
  passReset: PassReset = new PassReset();
  profile = new Profile();
  uploadingPicture: boolean = false;
  existsImgUser: boolean = false;
  destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private service : ServicesService,
    private formb : FormBuilder,
    private toastr : ToastrService,
    private metaTitleService: MetaTitleService,
  ) {
    this.metaTitleService.updateTitle();
  }

  ngOnInit() {
    this.viewProfile()
  }

  choosePic(){
    let native = this.pictureFile.nativeElement
    native.onchange = () => {
      if (native.files.length >= 1){
        this.beginUpload(native.files[0]);
      }
    };  
    native.click();
  }

  beginUpload(file: any){
    this.uploadingPicture = true;
    const formData = new FormData();
    formData.append('profile_picture', file);
    file.inProgress = true;
    this.service.uploadProfilePicture(formData)
    .pipe(
      map(event => {
        switch (event.type) {
          case HttpEventType.UploadProgress:
            file.progress = Math.round(event.loaded * 100 / event.total);
            break;
          case HttpEventType.Response:
            return event;
        }
      }))
      .subscribe(
        (event: any) => {
          if (typeof (event) === 'object') {
            if (event.body.profile_picture){
              const now = new Date();
              this.profile.profile_picture = event.body.profile_picture + '?' + Math.round(now.getTime() / 1000);
            }
            this.uploadingPicture = false;
          }
        },
        err => {
          file.inProgress = false;
          this.uploadingPicture = false;
          if(err.status == 400){
            this.toastr.error(err.error.profile_picture[0]);
          }else{
            this.toastr.error("No se puede cargar la imagen");
          }
        }
      );  
  }

  viewProfile(){
    return this.service.viewProfileService()
    .pipe(
      takeUntil(this.destroy$)
    )
    .subscribe((response : any)=>{
      console.log(response);
      this.profile = response;
      this.skeletonloader = false
      // let dataLength = data.length;
      // return data.substr(0,dataLength-1)+'-'+data.substr(dataLength-1,1)
      if (response.profile_picture==null) {
        // existsImgUser
        response.profile_picture = "../../../assets/images/users/1.jpg";
      }
      this.formProfile.patchValue(this.profile)
      },
      error =>{
        console.log(error)
      }
    )
  }
// 12345678
  savePass(){
    return this.service.passReset(this.formPass.value)
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(
        (response : any) =>{
          this.toastr.success('Contraseña restablecida con exito', 'Datos ingresados correctamente', { timeOut: 6000 });
        },
        error=>{
          // console.log(error)
          for (var element in error.error ){
            this.toastr.error(error.error[element],nameError[element]);
          } 
        }
      )
  }

  saveDataProfile(){
    return this.service.saveDataProfileService(this.formProfile.value)
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(
      (response : any )=>{
        this.profile.first_name = this.formProfile.value.first_name
        this.profile.last_name = this.formProfile.value.last_name

        this.toastr.success('actulización','cambio realizado correctamente');
        },
      error=>{
        var detalleError : any;
        switch(error.status){
          case 401:{ detalleError="Usuario y/o contraseña incorrecta" 
          break;} 
          case 400:{ detalleError="Datos incompletos" 
          break;} 
          case 500:{ detalleError="Error de servidor" 
          break;} 
          case 0:{ detalleError="No tiene acceso a internet" 
          break;} 
        }

        if (error.status == 400){
            for(var mnsj in error.error ){
            //  this.toastr.error('Error :'+ detalleError, error[mnsj]);
              this.toastr.error(mnsj,'Error :'+ detalleError);
            }
        }
      }
      )
  }

  ngOnDestroy(){
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
}
