import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-commons',
  // templateUrl: './profile.component.html',
  template: '<app-full-layout></app-full-layout>',
  styleUrls: ['./commons.component.scss']
})
export class CommonsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
