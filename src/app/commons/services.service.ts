import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { XhrErrorHandlerService } from "../services/xhr-error-handler.service";

@Injectable({
  providedIn: 'root'
})
export class ServicesService {

  private userInfo: any;
  constructor(
    private http : HttpClient,
    private xhrErrorHandlerService: XhrErrorHandlerService,
  ) { }


  public viewProfileService(){
    if(this.userInfo){
      return of(this.userInfo)
    }
    return this.http.get(
      environment.server_ip+'user/'
    ).pipe(
      tap(
        res => {
          this.userInfo = res;
        }
      )
    )
  }

  public saveDataProfileService(dataProfile){
    console.log(dataProfile)
    return this.http.put(
      environment.server_ip+'user/',dataProfile
      )
    }

  public uploadProfilePicture(picture: FormData){
    return this.http.put(
      environment.server_ip + 'user/picture/',
      picture,
      {
        reportProgress: true,
        observe: 'events'
      })
  }
    
  public passReset(dataPassReset){
      console.log(dataPassReset)
    return this.http.patch(
      environment.server_ip +'password/',dataPassReset
    )
  }

  public getWOtracking(code: Number) { 
    let params: any = { barcode: code };
    return this.http.get(
      environment.server_ip + 'wo/log/',
      { params: params }
    )
      .pipe(
        catchError(this.xhrErrorHandlerService.handleError)
      )
  }

  public getAnnotations(woId: number) {
    return this.http.get(
      environment.server_ip + 'wo/' + woId + '/annotations/'
    )
  }

  public addAnnotation(woId: number, data: any) {
    return this.http.post(
      environment.server_ip + 'wo/' + woId + '/annotations/',
      data
    ).pipe(
      catchError(this.xhrErrorHandlerService.handleError)
    )
  }

  public undoLastOp(woId: number){
    return this.http.post(
      environment.server_ip + 'wo/undo/',
      { wo : woId}
    ).pipe(
      catchError(this.xhrErrorHandlerService.handleError)
    )
  }
}