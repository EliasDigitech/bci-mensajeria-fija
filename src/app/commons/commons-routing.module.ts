import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewProfileComponent } from './view-profile/view-profile.component';
import { CommonsComponent } from './commons.component';
import { E404Component } from './e404/e404.component';
import { TrackingComponent } from './tracking/tracking.component';

const routes: Routes = [
  {
    path: 'profile',
    component: CommonsComponent,
    children: [
      {
        path: '',
        component: ViewProfileComponent,
        data: {
          title: 'Perfil'
        }
      },
    ]
  },{
    path: '404',
    component: E404Component,
    data: {
      title: '404'
    }
  },{
    path: 'tracking',
    component: TrackingComponent,
    data: {
      title: 'Seguimiento'
    }
  }
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
  ]
})
export class ProfileRoutingModule { }
