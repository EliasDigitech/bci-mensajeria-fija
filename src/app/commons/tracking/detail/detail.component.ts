import { Component, OnInit, Input, HostListener, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  @ViewChild('detailBody') detailBody: ElementRef;
  @Input() trackDetail: any;
  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if(event.keyCode == 44){
      console.log(this.detailBody.nativeElement)
      this.renderer2.addClass(this.detailBody.nativeElement, 'no-print')
      this.copyToClipboard("No es posible hacer capturas de pantalla de la firma")
    }
  }
  private copyToClipboard (str: string){
    const el = document.createElement('textarea');
    el.value = str;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
  };

  constructor(
    public activeModal: NgbActiveModal,
    private renderer2: Renderer2,
  ) { }

  ngOnInit() {
  }

}
