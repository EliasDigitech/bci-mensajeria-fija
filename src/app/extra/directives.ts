import { Directive, ChangeDetectorRef, Optional, OnInit, SimpleChange, Host, Input } from '@angular/core';
import { MaskDirective } from 'ngx-mask';
import { NgControl } from "@angular/forms";

@Directive({
    selector: '[chileanMask][mask]'
})
export class ChileanMaskDirective implements OnInit {
    private currentMask = "00 0 0000 0000"
    constructor(
        private control: NgControl,
        @Host() @Optional() 
        private mask: MaskDirective,
        private cdr: ChangeDetectorRef,
    ) { }
    
    ngOnInit() {
        this.mask.maskExpression = this.currentMask;
        this.mask.ngOnChanges({ "maskExpression": new SimpleChange("", this.currentMask, true) });
        this.mask.registerOnChange((val) => {
            this.calculateMask(val);
            this.control.control.setValue(val, { emitModelToViewChange: false });
        })
        this.cdr.detectChanges();
    }
    calculateMask(value) {
        let regionCode, regionPrefix, numberFormat;
        regionCode = value.startsWith(56) ? value.substring(2, 4) : value.substring(0, 2);
        if (regionCode.startsWith(2) || regionCode.startsWith(9)) {
            regionPrefix = '0';
            numberFormat = '0000 0000';
        } else {
            regionPrefix = '00';
            numberFormat = '000 0000';
        }
        let calculatedMask = `00 ${regionPrefix} ${numberFormat}`
        if (calculatedMask == this.currentMask) {
            return;
        }
        this.currentMask = calculatedMask;
        setTimeout(() => {
            this.mask.ngOnChanges({ "maskExpression": new SimpleChange(null, this.currentMask, false) });
        }, 50);
    }
}

@Directive({
    selector: '[disableControl]'
})
export class DisableControlDirective {

    @Input() set disableControl(condition: boolean) {
        const action = condition ? 'disable' : 'enable';
        this.ngControl.control[action]();
    }

    constructor(private ngControl: NgControl) {
    }

}
