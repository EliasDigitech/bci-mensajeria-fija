import { ValidatorFn, AbstractControl } from '@angular/forms';

export function rutValidator(): ValidatorFn {
    return (control: AbstractControl) : { [key: string]: any } | null => {
        if (control.value == null){
            return null;
        }
        let rut = control.value.substring(0, control.value.length -1);
        var M = 0, S = 1;
        let incomingVerifier = control.value.slice(control.value.length -1, control.value.length);
        for (; rut; rut = Math.floor(rut / 10))
             S = (S + rut % 10 * (9 - M++ % 6)) % 11;
        let expectedVerifier = S ? S - 1 : 'K';
        incomingVerifier = S?incomingVerifier:incomingVerifier.toUpperCase();

        return (incomingVerifier != expectedVerifier) || isNaN(rut)?{'invalidRut' : { value: control.value} } : null;
    }
}

// export function validateExistsAdrees(): ValidatorFn{
//     return (control: AbstractControl) :  { [key: string]: any } | null => { 
        
//         let Validate = undefined;
//         return (Validate === undefined)?{ 'invalidAdress' : { value: control.value} } : null;
//     }
// }

export class PasswordValidation {
    static MatchPassword(control: AbstractControl): { [key: string]: any } | null {
        let pass = control.get('password').value;
        let confirmPass = control.get('confirm_password').value;
        return pass === confirmPass ? null : { notSame: true }
    }
}
