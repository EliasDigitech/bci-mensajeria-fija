export class LoginData {
    username : string;
    password : string;  
}

export class TokenResponse {
    access : string;
    refresh : string;
}

export class ObtenerPass {
    email : string;
}

export class RestoreData {
    token : string;
    password : string;
    confirm_password : string;
}

