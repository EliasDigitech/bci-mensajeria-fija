import { Component, OnInit } from '@angular/core';
import { LoginData } from '../models';
import { AuthService } from '../services/auth.service';
import { Router } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import { MetaTitleService } from '../../services/meta-title.service';
// const StatusError ={
//   "401":"Usuario y/o contraseña incorrecta",
//   "400":"Datos incompletos",
//   "500":"Error de servidor",
//   "0":"No tiene acceso a internet",
// }

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginData : LoginData = new LoginData ();
  public isLoading: boolean = false;

  constructor(
    private authService:AuthService,
    private router : Router,
    private toastrService : ToastrService,
    private metaTitleService: MetaTitleService,
  ) {
    this.metaTitleService.updateTitle();
  }

  ngOnInit() {
  }

  login() {
    this.isLoading = true;
    this.authService.login(this.loginData)
    .subscribe(
      response => {
        if (response == true){
          this.authService.getSidebarElems()
            .subscribe(
              elems => {
                this.isLoading = false;
                // console.log(elems)
                if (elems.length > 0){
                  this.router.navigate([elems[0].path])
                }else{
                  this.toastrService.error("Perfil de usuario no habilitado");
                  localStorage.removeItem('jtoken');
                }
              }
            )
        }else{
          this.router.navigate(['auth/activate/']);
        }
      },
      error => { 
        this.isLoading = false;
        // console.log(error);
        var detalleError;
        switch(error.status){
          case 401:{ detalleError="Usuario y/o contraseña incorrecta" 
          break;} 
          case 400:{ detalleError="Datos incompletos" 
          break;} 
          case 500:{ detalleError="Error de servidor" 
          break;} 
          case 0:{ detalleError="No tiene acceso a internet" 
          break;} 
        }

        this.toastrService.error( detalleError,'Error de autenticación',{ positionClass: 'toast-top-left'});

      }
      )
    // console.log(this.loginData)
  }

}
