import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ObtenerPassComponent } from './obtener-pass/obtener-pass.component';
import { RestorePassComponent } from './restore-pass/restore-pass.component'
import { ActivateUserComponent } from './activate-user/activate-user.component'
import { AuthComponent } from './auth.component'

const routes: Routes = [
  {
    path: '', redirectTo: 'login', pathMatch: 'full'
  },
  {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent,
        data: {
          title: 'Login'
        }
      },
      {
        path: 'activate',
        component: ActivateUserComponent,
        data: {
          title: 'Actualizaciónd de datos'
        }
      },
    ]
  },
  {
    path: 'reset',
    component: ObtenerPassComponent,
    data: {
      title: 'Restaurar Contraseña'
    }
  },
  {
    path: 'restorepass',
    component: RestorePassComponent,
    data: {
      title: 'Restaurar Contraseña'
    }
  },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule { }
