import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { PasswordValidation, rutValidator } from '../../extra/validators';
import { AuthService } from '../services/auth.service';
// import { LayoutService } from '../../layouts/services/layout.service';
import { ToastrService } from 'ngx-toastr';
import { MetaTitleService } from '../../services/meta-title.service';

@Component({
  selector: 'app-activate-user',
  templateUrl: './activate-user.component.html',
  styleUrls: ['./activate-user.component.scss']
})
export class ActivateUserComponent implements OnInit {

  public user:any;
  private token: any;

  formPasswords = this.fb.group({
    // old_password: ['', Validators.required],
    password: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(50)]],
    confirm_password: ['', Validators.required],
    first_name: ['', Validators.required],
    last_name: ['', Validators.required],
    rut: ['', [Validators.required, rutValidator(), Validators.minLength(7)]],
    phone: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]],
  }, { validator: PasswordValidation.MatchPassword })

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService,
    // private layoutService: LayoutService,
    private toastrService: ToastrService,
    private metaTitleService: MetaTitleService,
  ) {
        this.metaTitleService.updateTitle();
      this.authService.retrieveUserInfo()
      .subscribe(
        data => {
          this.formPasswords.patchValue(data);
          this.token = localStorage.getItem('jtoken');
          localStorage.removeItem('jtoken');
        }
      )
  }

  ngOnInit() {
  }
  
  requestActivation(){
    this.authService.activateUser(this.formPasswords.value, this.token)
      .subscribe(
        () => {
          this.authService.getSidebarElems()
            .subscribe(
              elems => {
                if (elems.length > 0) {
                  this.router.navigate([elems[0].path])
                } else {
                  this.toastrService.error("Perfil de usuario no habilitado");
                }
              }
            )
        },
        error => {
          // console.log(error)
          if (error.errMsg && error.status != 500) {
            for (var errorHead in error.errMsg) {
              this.toastrService.error(error.errMsg[errorHead])
            }
          }
        }
      )
  }
}
