import { TestBed } from '@angular/core/testing';

import { XhrErrorHandlerService } from './xhr-error-handler.service';

describe('HttpHandlerErrorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: XhrErrorHandlerService = TestBed.get(XhrErrorHandlerService);
    expect(service).toBeTruthy();
  });
});
