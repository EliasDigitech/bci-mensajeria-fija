import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { TokenResponse, LoginData, ObtenerPass, RestoreData } from '../models';
import { environment } from '../../../environments/environment'
import { map, catchError, tap, switchMap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { XhrErrorHandlerService } from './xhr-error-handler.service';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private EXTRA_FIELDS = ['%company_id', '%branch_id']
  constructor(
    private http:HttpClient,
    private xhrErrorHandlerService:XhrErrorHandlerService,
    private router: Router,
    private jwtHelperService: JwtHelperService,
  ) { }

  public login(userInfo:LoginData){
    return this.http.post(
      environment.server_ip + 'login/',
      userInfo,
      {
        headers: {
          'Ingore-Refresh': 'ignore'
        }
      }
      ).pipe(
        map((response:TokenResponse) => {
          // console.log(this.jwtHelperService.decodeToken(response.access).status == 'disabled')
          localStorage.setItem('jtoken', response.access);
          if (this.jwtHelperService.decodeToken(response.access).status == 'disabled'){
            return false;
          }else{
            localStorage.setItem('rtoken', response.refresh);
            return true;
          }
        } ),
        catchError(this.xhrErrorHandlerService.handleError)
      )
  }

  public obtenerPasword(email:ObtenerPass){
    return this.http.post(
      environment.server_ip + 'reset/password/',
      email,
      {
        headers: {
          'Ingore-Refresh': 'ignore'
        }
      }
      )
      // return email;
      // .pipe(
      //   map((response:ObtenerPass) => {

      //     return response;
      //   } ),
      //   catchError(this.xhrErrorHandlerService.handleError)
      // )
  }

  public restorePass(dataRestore:RestoreData){
    return this.http.put(
      environment.server_ip + 'reset/password/',
      dataRestore,
      {
        headers: {
          'Ingore-Refresh': 'ignore'
        }
      }
    ).pipe(
      catchError(this.xhrErrorHandlerService.handleError)
    )
  }

  public retrieveUserInfo(){
    return this.http.get(
      environment.server_ip + 'user/',
    )
    .pipe(
      catchError(this.xhrErrorHandlerService.handleError)
    )
  }

  public activateUser(data:any, token){
    return this.http.post(
      environment.server_ip + 'activate/',
      data,
      {
        headers: {
          "Authorization": "Bearer "+ token
        }
      }
    )
    .pipe(
      tap(
        (response: TokenResponse) => {
          localStorage.setItem('jtoken', response.access);
        }
      ),
      catchError(this.xhrErrorHandlerService.handleError)
    )
  }

  public canAccessLogin():Observable<boolean>{
    let token = localStorage.getItem('jtoken');
    if(!token) return of(true);
    return this.http.post(
      environment.server_ip + 'verify/',
      { token : token}
    ).pipe(
      map(()=>{
        return false;
      }),
      catchError( () =>{
        localStorage.removeItem('jtoken');
        localStorage.removeItem('rtoken');
        return of(true);
      })
    )
  }

  getSidebarElems(): Observable<any> {
    return this.retrieveUserInfo()
      .pipe(
        switchMap(
          (userInfo:any) => {
            return this.http.get(environment.server_ip + 'render/')
              .pipe(
                map(
                  (res: any) => {
                    for (let el of res) {
                      let result = this.testValue(el.path);
                      if (result.result) {
                        el.path = el.path.replace(
                          result.chunk,
                          userInfo.branch[result.chunk.replace('%', '')]
                        )
                      }
                    }
                    return res;
                  }
                )
              )
          }
        ),
      )
  }

  private testValue(path: string): { result: boolean, chunk?: string } {
    for (let extra of this.EXTRA_FIELDS) {
      if (RegExp(extra).test(path)) {
        return { result: true, chunk: extra };
      }
    }
    return { result: false }
  }

  public logout():void {
    localStorage.removeItem('jtoken');
    localStorage.removeItem('rtoken');
    localStorage.removeItem('buttons');
    location.href = '/';
  }
}
