import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObtenerPassComponent } from './obtener-pass.component';

describe('ObtenerPassComponent', () => {
  let component: ObtenerPassComponent;
  let fixture: ComponentFixture<ObtenerPassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObtenerPassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObtenerPassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
