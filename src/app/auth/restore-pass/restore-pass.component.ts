import { Component, OnInit } from '@angular/core';
import { RestoreData } from '../models';
import { AuthService } from '../services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MetaTitleService } from '../../services/meta-title.service';

const FIELDS = {
  "password": "Contraseña",
  "confirm_password": "Confirmación"
}

@Component({
  selector: 'app-restore-pass',
  templateUrl: './restore-pass.component.html',
  styleUrls: ['./restore-pass.component.scss']
})
export class RestorePassComponent implements OnInit {

  restoreData: RestoreData = new RestoreData();
  isLoading: boolean = false;

  constructor(
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private toastrService: ToastrService,
    private metaTitleService: MetaTitleService,
  ) {
    this.metaTitleService.updateTitle();
  }
  // protected token: string;  

  ngOnInit() {
    // this.route.params.subscribe( params => this.token = params.token );
    this.route.queryParams.subscribe(params => {
      this.restoreData.token = params['token'];
      // this.restoreData.token = this.token;
      // console.log(params['token']);

    });

  }


  restorePass() {
    this.isLoading = true;
    this.authService.restorePass(this.restoreData)
      .subscribe(
        response => {
          this.isLoading = false;
          // console.log("ok");
          // console.log(response);
          this.toastrService.success('Contraseña restablecida con éxito', 'Datos ingresados correctamente', { timeOut: 6000 });
          this.router.navigate(['../login'], { relativeTo: this.route });
        },
        error => {
          this.isLoading = false;
          // console.log("error");
          // console.log(error);
          if (error.errMsg && error.status != 500) {
            for (let errHead in error.errMsg) {
              this.toastrService.error(error.errMsg[errHead], FIELDS[errHead]);
            }
          } else {
            this.toastrService.error("Ha ocurrido un problema");
          }
        }
      )
    // console.log(this.restoreData)

  }


}
