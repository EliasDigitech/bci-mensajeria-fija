export class Contacts {
    id?: number;
    full_name: string;
    rut?: string;
    email?: string;
    phone: string;
    address: string;
    comune: string;
    region: string;
    address_number?: string;
    companysearch?: number;
}