import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { SupervisorRoutingModule } from './supervisor-routing.module';
import { LayoutsModule } from '../layouts/layouts.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from '@auth0/angular-jwt';
import { NgxMaskModule, IConfig } from 'ngx-mask'
import { CompaniesModule } from '../admin/companies/companies.module'

import { SupervisorComponent } from './supervisor.component';

import { SupervisorService } from './services/supervisor.service';
import { ContactsListComponent } from './contacts/contacts-list/contacts-list.component';
import { SigleContactComponent } from './contacts/sigle-contact/sigle-contact.component';
// import { UsersModule } from "../users/users.module";
import { CommonsModule } from '../commons/commons.module';


export const options: Partial<IConfig> | (() => Partial<IConfig>) = {}

@NgModule({
  declarations: [
    SupervisorComponent,
    ContactsListComponent,
    SigleContactComponent,
  ],
  imports: [
    // UsersModule,
    CommonModule,
    SupervisorRoutingModule,
    LayoutsModule,
    CompaniesModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot(options),
    CommonsModule,
  ],
  providers: [
    SupervisorService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
  ]
})
export class SupervisorModule { }
