import { Component, OnInit, ViewChild, ElementRef, NgZone, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, Validators } from '@angular/forms';
import { rutValidator } from '../../../extra/validators';
import { MapsAPILoader } from '@agm/core';
import { SupervisorService } from '../../services/supervisor.service'
import { ToastrService } from "ngx-toastr";
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

const FIELDS = {
  full_name: "Nombre",
  rut: "RUT",
  email: "E-mail",
  phone: "Teléfono",
  address: "Dirección",
  comune: "Comuna",
  region: "Región",
  address_number: "Dpto./Oficina"
}

@Component({
  selector: 'app-sigle-contact',
  templateUrl: './sigle-contact.component.html',
  styleUrls: ['./sigle-contact.component.scss']
})
export class SigleContactComponent implements OnInit, OnDestroy {

  @ViewChild("addressSearch")
  public searchElementRef: ElementRef;
  public isNew: boolean;
  destroy$: Subject<boolean> = new Subject<boolean>();
  contactForm = this.fb.group({
    full_name: ['', Validators.required],
    rut: ['', [rutValidator(), Validators.minLength(7)]],
    phone: ['', Validators.required],
    address: ['', Validators.required],
    lat: ['', Validators.required],
    lng: ['', Validators.required],
    email: ['', [Validators.email]],
    comune: ['', Validators.required],
    region: ['', [Validators.required]],
    address_number: [''],
  })

  constructor(
    private supervisorService: SupervisorService,
    private toastrService: ToastrService,
    private Router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
  ) {
    this.route.data
      .pipe(
        takeUntil(this.destroy$)
      ).subscribe(
        data => {
          if (data.contact === true) {
            this.isNew = true;
          } else {
            this.isNew = false;
            console.log(data.contact);
            this.contactForm.patchValue(data.contact);
          }
        }
      )
  }

  ngOnInit() {
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(
        this.searchElementRef.nativeElement,
        {
          types: ["address"],
          componentRestrictions: { country: "cl" }
        }
      );
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          let components = place.address_components;
          let addressValues = {
            address: place.name,
            comune: components.find(el => el.types.includes("administrative_area_level_3")).long_name,
            region: components.find(el => el.types.includes("administrative_area_level_1")).long_name,
            lat: Math.round(place.geometry.location.lat() * 10000000) / 10000000,
            lng: Math.round(place.geometry.location.lng() * 10000000) / 10000000
          }
          this.contactForm.patchValue(addressValues);
        })
      });
    })
    this.contactForm.controls['address'].valueChanges
      .pipe(
        takeUntil(
          this.destroy$
        )
      )
      .subscribe(
        () => {
          this.contactForm.controls['comune'].reset()
          this.contactForm.controls['region'].reset()
        }
      )
  }
  createContact() {
    let idCompanie = this.route.snapshot.params['id']
    this.supervisorService.createContact(this.contactForm.value)
      .pipe(
        takeUntil(this.destroy$)
      ).subscribe(
        (response: any) => {
          this.toastrService.success('Datos guardados', '', { timeOut: 6000 });
          this.Router.navigate(['../'], { relativeTo: this.route });
        },
        error => {
          if (error.errMsg && error.status != 500) {
            for (let errHead in error.errMsg) {
              this.toastrService.error(error.errMsg[errHead], FIELDS[errHead]);
            }
          } else {
            this.toastrService.error("Ha ocurrido un problema");
          }
        }
      )
  }

  updateContact() {
    let contactId = this.route.snapshot.params['id'];
    this.supervisorService.updateContact(contactId, this.contactForm.value)
      .pipe(
        takeUntil(this.destroy$)
      ).subscribe((response: any) => {
        this.toastrService.success('', 'Cambios realizados correctamente');
        this.Router.navigate(['../'], { relativeTo: this.route });
      },
        error => {
          if (error.errMsg && error.status != 500) {
            for (let errHead in error.errMsg) {
              this.toastrService.error(error.errMsg[errHead], FIELDS[errHead]);
            }
          } else {
            this.toastrService.error("Ha ocurrido un problema");
          }
        }
      )
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}
