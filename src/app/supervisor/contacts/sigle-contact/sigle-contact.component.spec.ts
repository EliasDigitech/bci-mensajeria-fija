import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SigleContactComponent } from './sigle-contact.component';

describe('SigleContactComponent', () => {
  let component: SigleContactComponent;
  let fixture: ComponentFixture<SigleContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SigleContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SigleContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
