import { Component, OnInit, Renderer2, ViewChild, OnDestroy } from '@angular/core';
import { SupervisorService } from '../../services/supervisor.service';
// import { ListUsers } from '../../admin/models';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-contacts-list',
  templateUrl: './contacts-list.component.html',
  styleUrls: ['./contacts-list.component.scss']
})
export class ContactsListComponent implements OnInit, OnDestroy {
  @ViewChild(DataTableDirective) contactsTable: DataTableDirective;
  destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private supervisorService: SupervisorService,
    private router: Router,
    private route: ActivatedRoute,
    private renderer: Renderer2,
    private toastrService: ToastrService,
  ) { }

  ngOnInit() {
  }

  dtOptions: any = {
    dom: 'Bfrtip',
    serverSide: true,
    language: {
      url: '../../../assets/languages/Spanish.json'
    },
    processing: true,
    buttons: {
      buttons: [
        {
          text: 'Crear',
          key: '1',
          className: 'btn btn-primary btn-sm',
          action: (e, dt, node, config) => {
            this.router.navigate(['./new'], { relativeTo: this.route });
          }
        },{
          text: 'Excel',
          key: '1',
          className: 'btn btn-success btn-sm',
          action: (e, dt, node, config) => {
            this.router.navigate(['./new'], { relativeTo: this.route });
          }
        },{
          text: 'PDF',
          key: '1',
          className: 'btn btn-danger btn-sm',
          action: (e, dt, node, config) => {
            this.router.navigate(['./new'], { relativeTo: this.route });
          }
        }
      ]
    },
    columns: [
      {
        title: 'Nombre',
        data: 'full_name'
      }, {
        title: 'RUT',
        data: 'rut',
        render: function (data: any, type: any, full: any) {
          if(data){
          let dataLength = data.length
            return data.substr(0,dataLength-1)+'-'+data.substr(dataLength-1,1)
          }else{
            return data
          }
        }
      }, {
        title: 'Dirección',
        data: 'address'
      }, {
        title: 'Comuna',
        data: 'comune'
      }, {
        title: 'Región',
        data: 'region'
      },{
        title: 'Acción',
        data: 'id',
        orderable: false,
        className: 'text-center',
        render: function (data: any, type: any, full: any) {
          return `<span>
                <button type="button" class="btn btn-sm btn-info mr-3" idEditContact="${data}"><i class="fas fa-edit" idEditContact="${data}"></i></button>
                <button type="button" class="btn btn-sm btn-danger" idDeleteContact="${data}">
                  <i class="fas fa-times-circle" idDeleteContact="${data}"></i>
                </button>
                </span>`

        }
      }
    ],
    ajax: (data: any, callback) => {
      let order = data.order[0];
      let consulta: any = {
        search: data.search.value,
        offset: data.start,
        limit: data.length,
        ordering: (order.dir == 'desc' ? '-' : '') + data.columns[order.column].data
      };
      var idCompanie = this.route.snapshot.params['id']
      this.supervisorService.listContacts(consulta)
        .pipe(
          takeUntil(this.destroy$)
        ).subscribe(
          (response: any) => {
            callback({
              recordsTotal: response.count,
              recordsFiltered: response.count,
              data: response.results
            });
          },
          error => {
            this.toastrService.error("Ha ocurrido un error al mostrar la lista de contactos");
          }
        )
    },
    rowCallback: (row: Node, data: any[] | Object, index: number) => {
      const self = this;
      $('td', row).unbind('click');
      $('td', row).bind('click', event => {
        if (event.target.hasAttribute("idEditContact")) {
          self.router.navigate(['./' + event.target.getAttribute("idEditContact")], { relativeTo: self.route });
        }
        if (event.target.hasAttribute("idDeleteContact")) {
          let r = confirm("¿Está seguro de eliminar el contacto?");
          if(r){
            self.supervisorService.deleteContact(event.target.getAttribute("idDeleteContact"))
              .subscribe(
                ()=>{
                  self.toastrService.success("Contacto eliminado exitosamente");
                  console.log(self.contactsTable)
                  self.contactsTable.dtInstance.then((dtInstance: DataTables.Api) => {
                    dtInstance.ajax.reload();
                  })
                },
                err => {
                  self.toastrService.error("No es posible eliminar el contacto");
                  console.log(err);
                }
              )
          }
        }
      });
      return row;
    },
  };

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}
