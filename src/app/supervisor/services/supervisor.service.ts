import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { XhrErrorHandlerService } from '../../services/xhr-error-handler.service'
import { catchError } from 'rxjs/operators';
import { Contacts } from '../models';

@Injectable()
export class SupervisorService {

  constructor(
    private http: HttpClient,
    private xhrErrorHandlerService: XhrErrorHandlerService,
  ) { }

  // contacts related
  public listContacts(params: any) {
    return this.http.get(
      environment.server_ip + 'autocomplete/',
      { params: params })
      .pipe(
        catchError(this.xhrErrorHandlerService.handleError)
      )
  }

  public getContact(id: Number) {
    return this.http.get(
      environment.server_ip + 'autocomplete/' + id + '/')
      .pipe(
        catchError(this.xhrErrorHandlerService.handleError)
      )
  }

  public createContact(data: Contacts) {
    return this.http.post(
      environment.server_ip + 'autocomplete/', data)
      .pipe(
        catchError(this.xhrErrorHandlerService.handleError)
      )
  }

  public updateContact(id: Number, data: Contacts) {
    return this.http.put(
      environment.server_ip + 'autocomplete/' + id + '/', data)
      .pipe(
        catchError(this.xhrErrorHandlerService.handleError)
      )
  }

  public deleteContact(id: Number | string) {
    return this.http.delete(
      environment.server_ip + 'autocomplete/' + id + '/')
      .pipe(
        catchError(this.xhrErrorHandlerService.handleError)
      )
  }

  public viewCompany(id){
    return this.http.get(
      environment.server_ip + 'companies/'+id+'/'
    )
  }
}
