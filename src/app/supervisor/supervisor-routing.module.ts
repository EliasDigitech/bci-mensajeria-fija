import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SupervisorComponent } from './supervisor.component';

import { SupervisorContactResolver } from './services/company-supervisor.resolver';
import { ContactsListComponent } from './contacts/contacts-list/contacts-list.component';
import { SigleContactComponent } from './contacts/sigle-contact/sigle-contact.component';
import { WoListComponent } from '../layouts/shared/wo-list/wo-list.component';
import { TrackingComponent } from '../commons/tracking/tracking.component';
import { SolicitudOtComponent } from '../layouts/shared/solicitud-ot/solicitud-ot.component';
import { WOResolver } from './services/wo.resolve'
import { RouteGuard } from '../services/route.guard';

const routes: Routes = [
  {
    path:'',
    component: SupervisorComponent,
    children: [
    {
      path: 'contacts',
      component: ContactsListComponent,
      data: { segment: 'supervisor/contacts' },
    },{
      path: 'contacts/:id',
      component: SigleContactComponent,
      data: { segment: 'supervisor/contacts' },
      resolve: { contact: SupervisorContactResolver}
    },{
      path: 'wolist',
      component: WoListComponent,
      data: { group: 'Y29tcGFueV9kbXM=' }
    },{
      path: 'wolist/tracking',
      component: TrackingComponent
    },{
      path: 'solicitudot/:id',
      component: SolicitudOtComponent,
      resolve: { wo: WOResolver },
      data: { group: 'dXNlcnM=' }
    }
  ]
  }, {
    path: 'companies',
    component: SupervisorComponent,
    loadChildren: () => import('../admin/companies/companies.module').then(m => m.CompaniesModule),
    data: {
      segment: '/supervisor/companies/%company_id/',
      group: 'dXNlcnM='
    },
    canLoad: [RouteGuard],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    WOResolver,
    SupervisorContactResolver,
  ]
})
export class SupervisorRoutingModule { }
