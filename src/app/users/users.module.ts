import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule} from './users-routing';
import { UsersComponent } from './users.component';
import { LayoutsModule } from '../layouts/layouts.module'; 
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { ListOtComponent } from './ot/list-ot/list-ot.component';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import { NgxMaskModule, IConfig } from 'ngx-mask'
import { CommonsModule } from '../commons/commons.module';
import { CurrencyMaskModule } from "ng2-currency-mask"; 

import { ServicesService } from './services.service';
import { RouteReuseStrategy } from '@angular/router';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = {}

@NgModule({
  declarations: [
    UsersComponent,
    ListOtComponent,
  ],
  imports: [
    CommonModule,
    CurrencyMaskModule,
    AdminRoutingModule,
    LayoutsModule,
    FormsModule,
    ReactiveFormsModule,
    AutocompleteLibModule,
    NgxMaskModule.forRoot(options),
    CommonsModule, 
  ],
  providers: [
    ServicesService,
  ]
})
export class UsersModule { }
