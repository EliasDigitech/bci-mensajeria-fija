import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { environment } from '../../../environments/environment'
import { XhrErrorHandlerService } from "../../services/xhr-error-handler.service";
import { catchError } from 'rxjs/operators';

@Injectable()
export class TraderService {
    constructor(
        private http: HttpClient,
        private xhrErrorHandlerService: XhrErrorHandlerService,
    ) { }

    public listFicha(consulta: any) {
        return this.http.get(
            environment.server_ip + 'company_files/', { params: consulta }
        )
    }

    public historialFicha(id: number) {
        return this.http.get(
            environment.server_ip + 'company_files/' + id + '/'
        )
    }
}