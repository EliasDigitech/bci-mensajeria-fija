import { Component, OnInit,ViewChild,ElementRef,Renderer2 } from '@angular/core';
import { TraderService } from '../services/trader.service'
import { ActivatedRoute, Router } from "@angular/router";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MetaTitleService } from '../../services/meta-title.service';

@Component({
  selector: 'app-listadofichas',
  templateUrl: './listadofichas.component.html',
  styleUrls: ['./listadofichas.component.scss']
})

  export class ListadofichasComponent implements OnInit {
    
  @ViewChild('listadoTable') listadoTable : ElementRef;
  @ViewChild('contentModal') contentModal : ElementRef;
  
  datoModal : any;
    
  constructor(
    private traderService: TraderService,
    private router : Router,
    private route: ActivatedRoute,
    private renderer: Renderer2,
    private modalService: NgbModal,
    private metaTitleService: MetaTitleService,
  ) {
    this.metaTitleService.updateTitle();
  } 

  dtOptions: any = {
    dom: 'Bfrtip',
    serverSide: true,
    processing: true,
    language: {
      url: '../../../assets/languages/Spanish.json'
    },
    buttons: {
      buttons:[
           {
            text: 'Crear',
            key: '1',
            className: 'btn btn-primary btn-sm',
            action: (e, dt, node, config) => {
              this.router.navigate(['../files/new'],{relativeTo : this.route});
             }
           },
       ]
    },
    columns: [
      {
        title: 'Nombre contacto',
        data:  null,
        render: function (data: any, type: any, full: any) {
          return data.contact_first_name +" "+ data.contact_last_name
        }
      },
      {
        title: 'Compañía', 
        data: 'company_name',
        render: function (data: any, type: any, full: any) {
          if(data){
            return data[0].toUpperCase() + data.slice(1);
          }else{
            return data;
          }
        }
      },{
        title: 'Rut',
        data: 'rut',
        render: function (data: any, type: any, full: any) {
          if(data){
          let dataLength = data.length
            return data.substr(0,dataLength-1)+'-'+data.substr(dataLength-1,1)
          }else{
            return data
          }
        }
      },{
        title: 'Dirección',
        data: 'address'
      },{
        title: 'Comuna',
        data: 'comune'
      },{
        title: 'Último estado',
        data: 'status',
        render: function (data: any, type: any, full: any) {
          switch (data) {
            case 'draft': {
              return 'Borrador'
              break;
            }
            case 'sent': {
              return 'Enviada'
              break;
            }
            case 'rejected': {
              return 'Rechazada'
              break;
            }
            case 'accepted': {
              return 'Aprobada'
              break;
            }
          }
        }
      },{
        title: 'Editar',
        data: 'id',
        orderable: false,
        className: 'text-center',
        render: function (data: any, type: any, full: any) {
          return `<button class="btn btn-info btn-sm" idEditUser="${data}">
                    <i class="fas fa-eye" idEditUser="${data}"></i>
                  </button" >`;
        }
      },{
        title: 'Historial',
        data: 'id',
        className: 'text-center',
        orderable: false,
        render: function (data: any, type: any, full: any) {
        return  `<button type="button" idBrancheRedirectUser="${data}" class="btn btn-primary btn-sm" ><i idBrancheRedirectUser="${data}" class="fa fa-history"></i></button>`
        }
      },

    ],
    ajax : (data : any, callback ) => {
      let order = data.order[0];
      let consulta:any = {
        search: data.search.value,
        offset: data.start,
        limit : data.length,
        ordering: (order.dir == 'desc'?'-':'') + data.columns[order.column].data
      };
      
      this.traderService.listFicha(consulta)
      .subscribe(
        (response: any) => {
          // console.log(response);
          callback({
            recordsTotal: response.count,
            recordsFiltered: response.count,
            data: response.results
          });
        },
          error => {
            // console.log(error);
        }
      )
    },
  };

  ngAfterViewInit(): void {
    this.renderer.listen(this.listadoTable.nativeElement,'click', (event) => {
      // console.log(event);
      if (event.target.hasAttribute("idEditUser")) {
        this.router.navigate(['../files/' + event.target.getAttribute("idEditUser")],{relativeTo : this.route});
      }
      else if(event.target.hasAttribute("idBrancheRedirectUser")){
        this.openModal(this.contentModal, event.target.getAttribute("idBrancheRedirectUser"))
      }

    });
  }

  openModal(content, id) {
    this.traderService.historialFicha(id).subscribe(
        (response : any)=>{
          this.datoModal=response;
          for(let estadoficha of this.datoModal.log){
            switch(estadoficha.status) { 
              case 'draft': { 
                estadoficha.status = 'Borrador'
                 break; 
              } 
              case 'sent': { 
                estadoficha.status = 'Enviada'
                 break; 
              }  
              case 'rejected': { 
                estadoficha.status = 'Rechazada' 
                 break; 
              }
              case 'accepted': { 
                estadoficha.status = 'Aprobada'
                break; 
             } 
           } 
          }

        this.modalService.open(content);
    },
    error=>{
      // console.log(error);
    }
    )    
  }

  ngOnInit() {   
  }
  
}
