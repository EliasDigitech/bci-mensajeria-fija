import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginGuard } from './services/login.guard';
import { RouteGuard } from './services/route.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'auth',
    pathMatch: 'full'
  },
  { path: 'auth',
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule),
    canLoad: [LoginGuard]
  },
  { path: 'adm',
    loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule)
  },
  { path: 'users',
    loadChildren: () => import('./users/users.module').then(m => m.UsersModule),
    data: { segment: '/users/solicitudot/new/' },
    canLoad: [RouteGuard]
  },  
  { path: 'supervisor',
    loadChildren: () => import('./supervisor/supervisor.module').then(m => m.SupervisorModule),
    data: { segment: '/supervisor/companies/%company_id/' },
    canLoad: [RouteGuard]
  },
  { path: 'operator',
    loadChildren: () => import('./operator/operator.module').then(m => m.OperatorModule),
    data: { segment: '/operator/dashboard/' },
    canLoad: [RouteGuard]
  },
  {
    path: 'trader',
    loadChildren: () => import('./trader/trader.module').then(m => m.TraderModule),
    data: { segment: '/trader/files' },
    canLoad: [RouteGuard]
  },
  { path: '',
    loadChildren: () => import('./commons/commons.module').then(m=> m.CommonsModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [RouteGuard,]
})
export class AppRoutingModule { }
