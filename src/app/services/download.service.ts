import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse  } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class DownloadService {

  constructor(
    private http: HttpClient,
  ) { }

  downloadReport(path: string, params: any):Observable<Blob>{
    return this.http.get(environment.server_ip + path, { params: params, responseType: 'blob' })
  }
}