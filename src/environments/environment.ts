// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  //server_ip : "http://localhost:8000/",
  // server_ip : "http://api.postal.digitechqa.cl/",
  server_ip: "http://localhost:8000/",
  server_ipWebSocket: "ws://localhost/",

  maps_api_key: "AIzaSyB9s3_OH9JQmf2gor_44nYJAOzdfSTW33Q" // Javier Vallenilla Key
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
